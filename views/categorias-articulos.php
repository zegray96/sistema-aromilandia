<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: login');
} else {
    // HEADER
    require('layouts/header.php');
    // END HEADER?>

<!-- Container fluid -->
<div class="container-fluid" id="container-wrapper">

    <?php
        if ($_SESSION['v_categorias_art']==0) {
            echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
        } else { ?>

    <div class="justify-content-between mb-4">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-clipboard-list"></i> Categorias de Articulos</h1>
            </div>
            <div class="col-sm-4">
                <div class="btn-group float-right mt-3 mt-sm-0" role="group">
                    <?php
                        if ($_SESSION['new_categorias_art']==1) {
                            echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
                        } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div id="contenedor-cabecera"
                    class="card-header py-1 d-flex flex-row align-items-center justify-content-between">
                    <!--  -->
                </div>
                <!-- Tabla -->
                <div id="listado" class="table-responsive p-3">
                    <table id="tblListado" class="table align-items-center table-hover table-bordered"
                        style="width: 100%;">
                        <thead class="thead-light">
                            <th><i class="fas fa-th-list ml-2 mr-2"></i></th>
                            <th>&nbsp;Nombre&nbsp;</th>
                        </thead>
                    </table>
                </div>
                <!-- End tabla -->

                <!-- Formulario -->
                <div id="formulario">
                    <div class="card-body">
                        <form id="form">

                            <input type="hidden" id="idCategoria" name="idCategoria">

                            <div class="form-group">
                                <label><span class="text-danger">(*)</span> Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" data-maxsize="50"
                                    required autocomplete="off" style="text-transform: capitalize;">
                            </div>



                            <div class="mt-4 mb-4 float-right">
                                <button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
                                <button type="submit" id="btnGuardar" class="btn btn-primary ml-3">Guardar</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- End formulario -->
            </div>
        </div>
    </div>

    <?php } ?>

</div>
<!-- End Container fluid -->
</div>
<!-- End Content -->

<?php
// FOOTER
require('layouts/footer.php')
// END FOOTER
?>

<!-- VIEW SCRIPT -->
<script src="../js/categoriaArticulo.js?ver=<?php echo VERSION?>"></script>

<?php
}
ob_end_flush();