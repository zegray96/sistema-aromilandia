<!-- Jquery -->
<script src="../assets/js/jquery/jquery.min.js"></script>
<!-- Jquery easing -->
<script src="../assets/js/jquery-easing/jquery.easing.min.js"></script>
<!-- Bootstrap con modificaciones -->
<script src="../assets/js/bootstrap/bootstrap.bundle.min.js"></script>
<!-- Template Ruang -->
<script src="../assets/js/ruang-admin/ruang-admin.min.js"></script>

<!-- Datatables -->
<script src="../assets/js/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/js/datatables/dataTables.bootstrap4.min.js"></script>
<script src="../assets/js/datatables/buttons.print.min.js"></script>
<script src="../assets/js/datatables/dataTables.buttons.min.js"></script>
<script src="../assets/js/datatables/jszip.min.js"></script>
<script src="../assets/js/datatables/pdfmake.min.js"></script>
<script src="../assets/js/datatables/vfs_fonts.js"></script>
<script src="../assets/js/datatables/buttons.html5.min.js"></script>

<!-- Sweetalert2 -->
<script src="../assets/js/sweetalert2/sweetalert2.all.min.js"></script>

<!-- Maxlength -->
<script src="../assets/js/maxlength/maxlength.js"></script>

<!-- Bootstrap Datepicker -->
<script src="../assets/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="../assets/js/bootstrap-datepicker/bootstrap-datepicker.es.min.js"></script>

<!-- Bootstrap select -->
<script src="../assets/js/bootstrap-select/bootstrap-select.min.js"></script>
<!-- Idioma bootstrap-select -->
<script src="../assets/js/bootstrap-select/defaults-es_ES.js"></script>