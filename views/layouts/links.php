<!-- FontAwesome -->
<link rel="stylesheet" href="../assets/css/fontawesome/all.min.css">
<!-- Bootstrap con modificaciones -->
<link rel="stylesheet" href="../assets/css/bootstrap/bootstrap.min.css">
<!-- Template Ruang -->
<link rel="stylesheet" href="../assets/css/ruang-admin/ruang-admin.min.css">
<!-- Datatables -->
<link rel="stylesheet" href="../assets/css/datatables/dataTables.bootstrap4.min.css">
<!-- Bootstrap DatePicker -->
<link rel="stylesheet" href="../assets/css/bootstrap-datepicker/bootstrap-datepicker.min.css">
<!-- Bootstrap select -->
<link rel="stylesheet" href="../assets/css/bootstrap-select/bootstrap-select.min.css">
<!-- Styles -->
<link rel="stylesheet" href="../assets/css/styles.css?ver=<?php echo VERSION?>">