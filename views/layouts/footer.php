      <!--Modal Cargando -->
      <div class="modal" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
          aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <div class="spinner-border cargando-modal"></div>
                  </div>
              </div>
          </div>
      </div>
      <!--Fin Modal Cargando -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
          <div class="container my-auto">
              <div class="copyright text-center my-auto">
                  <span>Version <?php echo VERSION?>

                  </span>
              </div>
          </div>
      </footer>
      <!-- Footer -->
      </div>
      <!-- End Content wrapper -->
      </div>
      <!-- End wrapper -->

      <!-- Scroll to top -->
      <a class="scroll-to-top rounded" href="#page-top">
          <i class="fas fa-angle-up"></i>
      </a>

      <!-- SCRIPTS -->
      <?php require('scripts.php') ?>
      <!-- END SCRIPTS -->

      <!-- SESSION SCRIPT -->
      <script src="../js/sesion.js?ver=<?php echo VERSION?>"></script>

      </body>

      </html>