<?php
    require('../config/env.php');
    require('../models/Configuracion.php');
    $conf=new Configuracion();
    $respuesta=$conf->traer_configuracion();
    $nombreEmpresa=$respuesta['nombre_empresa'];
?>
<!DOCTYPE html>
<html>

<head lang="es">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title id="tituloPagina"></title>
    <!-- ICONO PAGINA -->
    <link id="iconoPagina" href="../assets/img/icono.ico?<?php $aleatorio=rand(); echo $aleatorio ?>" rel="icon">
    <!-- LINKS CSS -->
    <?php require('links.php') ?>
    <!-- END LINKS CSS -->
</head>

<body id="page-top">
    <!-- Preload -->
    <div id="preload">
        <div class="spinner-border"></div>
    </div>
    <!-- Fin preload -->

    <!-- Wrapper -->
    <div id="wrapper" style="display: none;">
        <!-- Sidebar Menu -->
        <?php require('menu.php') ?>
        <!-- End Sidebar Menu-->

        <!-- Content-wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Content -->
            <div id="content">
                <!-- TopBar -->
                <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
                    <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <span class="ml-2 text-white name-user">
                        <?php echo $_SESSION['sucursal'];?>
                    </span>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="img-user" src="../assets//img/user.png">
                                <span id="spanApellidoNombreUsu" class="ml-2 d-none d-lg-inline text-white name-user">
                                    <?php echo $_SESSION['apellidoNombre'];?>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="mi-cuenta">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Mi Cuenta
                                </a>

                                <?php if ($_SESSION['v_configuracion']==1) { ?>
                                <a class="dropdown-item" href="configuracion">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Configuración
                                </a>

                                <?php } ?>

                                <div class="dropdown-divider"></div>
                                <a id="btnCerrarSesion" class="dropdown-item cerrar-sesion">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Cerrar Sesión
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- Topbar -->