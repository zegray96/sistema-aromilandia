<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
        <div class="sidebar-brand-icon">
            <img id="logoHeader" src="../assets//img/logo-con-borde.png?<?php $aleatorio=rand(); echo $aleatorio ?>">
        </div>
        <div id="nombreEmpresaHeader" class="sidebar-brand-text mx-2"><?php echo $nombreEmpresa; ?>
        </div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item active">
        <a class="nav-link" href="dashboard">
            <i class="fas fa-home"></i>
            <span>Dashboard</span></a>
    </li>
    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Secciones
    </div>

    <!-- Acceso -->
    <?php if ($_SESSION['v_acceso']==1) { ?>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#acceso" aria-expanded="true"
            aria-controls="collapseTable">
            <i class="fas fa-user-lock"></i>
            <span>Acceso</span>
        </a>
        <div id="acceso" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="usuarios"><i class="fas fa-user"></i> Usuarios</a>
                <a class="collapse-item" href="roles"><i class="fas fa-user-cog"></i> Roles</a>
            </div>
        </div>
    </li>

    <?php } ?>

    <!-- Parametros -->
    <?php  if ($_SESSION['v_categorias_art']==1 || $_SESSION['v_metodos_pago']==1) { ?>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#parametros" aria-expanded="true"
            aria-controls="collapseTable">
            <i class="fas fa-cog"></i>
            <span>Parametros</span>
        </a>
        <div id="parametros" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <!-- categorias articulos -->
                <?php
                        if ($_SESSION['v_categorias_art']==1) {
                            ?>
                <a class="collapse-item" href="categorias-articulos"><i class="fas fa-clipboard-list"></i>
                    Categorias Articulos</a>

                <?php
                        } ?>

                <!-- metodos de pago -->
                <?php if ($_SESSION['v_metodos_pago']==1) { ?>
                <a class="collapse-item" href="metodos-pago"><i class="fas fa-dollar-sign"></i> Metodos de
                    Pago</a>

                <?php } ?>

            </div>
        </div>
    </li>

    <?php } ?>

    <!-- Clientes -->
    <?php if ($_SESSION['v_clientes']==1) { ?>

    <li class="nav-item">
        <a class="nav-link" href="clientes">
            <i class="fas fa-users"></i>
            <span>Clientes</span>
        </a>
    </li>

    <?php } ?>

    <!-- Articulos -->
    <?php if ($_SESSION['v_articulos']==1) { ?>

    <li class="nav-item">
        <a class="nav-link" href="articulos">
            <i class="fas fa-box-open"></i>
            <span>Articulos</span>
        </a>
    </li>

    <?php } ?>


    <!-- Presupuestos -->
    <?php if ($_SESSION['v_presupuestos']==1) { ?>

    <li class="nav-item">
        <a class="nav-link" href="presupuestos">
            <i class="fas fa-dollar-sign"></i>
            <span>Presupuestos</span>
        </a>
    </li>

    <?php } ?>


    <hr class="sidebar-divider">
    <div class="version">Version <?php echo VERSION?>
    </div>

</ul>