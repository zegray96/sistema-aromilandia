<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: login');
} else {
    // HEADER
    require('layouts/header.php');
    // END HEADER?>

<!-- Container fluid -->
<div class="container-fluid" id="container-wrapper">

    <?php
        if ($_SESSION['v_acceso']==0) {
            echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
        } else { ?>

    <div class="justify-content-between mb-4">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-users-cog"></i> Roles</h1>
            </div>
            <div class="col-sm-4">
                <div class="btn-group float-right mt-3 mt-sm-0" role="group">
                    <?php
                        if ($_SESSION['new_articulos']==1) {
                            echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
                        } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div id="contenedor-cabecera"
                    class="card-header py-1 d-flex flex-row align-items-center justify-content-between">
                    <!--  -->
                </div>
                <!-- Tabla -->
                <div id="listado" class="table-responsive p-3">
                    <table id="tblListado" class="table align-items-center table-hover table-bordered"
                        style="width: 100%;">
                        <thead class="thead-light">
                            <th><i class="fas fa-th-list ml-2 mr-2"></i></th>
                            <th>&nbsp;Nombre&nbsp;</th>
                        </thead>
                    </table>
                </div>
                <!-- End tabla -->

                <!-- Formulario -->
                <div id="formulario">
                    <div class="card-body">
                        <form id="form">

                            <input type="hidden" id="idRol" name="idRol">

                            <div class="form-group">
                                <label><span class="text-danger">(*)</span> Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" data-maxsize="50"
                                    required autocomplete="off" style="text-transform: capitalize;">
                            </div>

                            <div class="form-group">
                                <label>Vistas</label>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="vConfiguracion"
                                        name="vConfiguracion">
                                    <label class="custom-control-label" for="vConfiguracion">Configuración</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="vAcceso" name="vAcceso">
                                    <label class="custom-control-label" for="vAcceso">Acceso</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="vClientes" name="vClientes">
                                    <label class="custom-control-label" for="vClientes">Clientes</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="vArticulos"
                                        name="vArticulos">
                                    <label class="custom-control-label" for="vArticulos">Articulos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="vCategoriasArt"
                                        name="vCategoriasArt">
                                    <label class="custom-control-label" for="vCategoriasArt">Categorias
                                        Articulos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="vMetodosPago"
                                        name="vMetodosPago">
                                    <label class="custom-control-label" for="vMetodosPago">Metodos de Pagos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="vPresupuestos"
                                        name="vPresupuestos">
                                    <label class="custom-control-label" for="vPresupuestos">Presupuestos</label>
                                </div>



                            </div>

                            <div class="form-group">
                                <label>Nuevos registros</label>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="newAcceso" name="newAcceso">
                                    <label class="custom-control-label" for="newAcceso">Acceso</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="newClientes"
                                        name="newClientes">
                                    <label class="custom-control-label" for="newClientes">Clientes</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="newArticulos"
                                        name="newArticulos">
                                    <label class="custom-control-label" for="newArticulos">Articulos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="newCategoriasArt"
                                        name="newCategoriasArt">
                                    <label class="custom-control-label" for="newCategoriasArt">Categorias
                                        Articulos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="newMetodosPago"
                                        name="newMetodosPago">
                                    <label class="custom-control-label" for="newMetodosPago">Metodos de Pagos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="newPresupuestos"
                                        name="newPresupuestos">
                                    <label class="custom-control-label" for="newPresupuestos">Presupuestos</label>
                                </div>



                            </div>

                            <div class="form-group">
                                <label>Alterar registros (Editar)</label>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="altAcceso" name="altAcceso">
                                    <label class="custom-control-label" for="altAcceso">Acceso</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="altClientes"
                                        name="altClientes">
                                    <label class="custom-control-label" for="altClientes">Clientes</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="altArticulos"
                                        name="altArticulos">
                                    <label class="custom-control-label" for="altArticulos">Articulos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="altCategoriasArt"
                                        name="altCategoriasArt">
                                    <label class="custom-control-label" for="altCategoriasArt">Categorias
                                        Articulos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="altMetodosPago"
                                        name="altMetodosPago">
                                    <label class="custom-control-label" for="altMetodosPago">Metodos de Pagos</label>
                                </div>


                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="altPresupuestos"
                                        name="altPresupuestos">
                                    <label class="custom-control-label" for="altPresupuestos">Presupuestos</label>
                                </div>

                            </div>

                            <div class="form-group">
                                <label>Eliminar Registros</label>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="delPresupuestos"
                                        name="delPresupuestos">
                                    <label class="custom-control-label" for="delPresupuestos">Presupuestos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="delCategoriasArt"
                                        name="delCategoriasArt">
                                    <label class="custom-control-label" for="delCategoriasArt">Categorias
                                        Articulos</label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="delMetodosPago"
                                        name="delMetodosPago">
                                    <label class="custom-control-label" for="delMetodosPago">Metodos de Pagos</label>
                                </div>


                            </div>

                            <div class="mt-4 mb-4 float-right">
                                <button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
                                <button type="submit" id="btnGuardar" class="btn btn-primary ml-3">Guardar</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- End formulario -->
            </div>
        </div>
    </div>

    <?php } ?>

</div>
<!-- End Container fluid -->
</div>
<!-- End Content -->

<?php
// FOOTER
require('layouts/footer.php')
// END FOOTER
?>

<!-- VIEW SCRIPT -->
<script src="../js/rol.js?ver=<?php echo VERSION ?>"></script>

<?php
}
ob_end_flush();