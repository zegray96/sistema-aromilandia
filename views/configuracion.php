<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: login');
} else {
    // HEADER
    require('layouts/header.php');
    // END HEADER?>

<!-- Container fluid -->
<div class="container-fluid" id="container-wrapper">
    <?php
        if ($_SESSION['v_configuracion']==0) {
            echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
        } else { ?>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-cog"></i> Configuración</h1>
    </div>

    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <!-- Formulario -->
                <div id="formulario">
                    <div class="card-body">
                        <form id="form">

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label><span class="text-danger">(*)</span> Nombre Empresa</label>
                                    <input type="text" class="form-control" id="nombreEmpresa" name="nombreEmpresa"
                                        data-maxsize="40" required style="text-transform: capitalize;"
                                        autocomplete="off">
                                </div>

                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label>Logo Empresa <span class="text-danger">(.png)</span></label>
                                    <input type="file" class="form-control" name="logoEmpresa" id="logoEmpresa">


                                    <div class="mt-3 div-img-logo-icono">
                                        <img id="logoEmpresaImg" src="../assets/img/logo-empresa.png" width="150px"
                                            height="150px">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label><span class="text-danger">(*)</span> Compra Minima Mayorista</label>
                                    <input type="text" class="form-control" id="compraMinimaMayorista"
                                        name="compraMinimaMayorista" placeholder="00.0" autocomplete="off">
                                </div>

                                <div class="col-12 col-md-6">
                                    <label><span class="text-danger">(*)</span> Siguientes Compras Mayoristas</label>
                                    <input type="text" class="form-control" id="siguientesComprasMayoristas"
                                        name="siguientesComprasMayoristas" placeholder="00.0" autocomplete="off">
                                </div>

                            </div>



                            <div class="mb-4 float-right botones-con-margen" style="">
                                <button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
                                <button type="submit" id="btnGuardar" class="btn btn-primary ml-3">Guardar</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- End formulario -->
            </div>
        </div>
    </div>

    <?php } ?>

</div>
<!-- End Container fluid -->
</div>
<!-- End Content -->

<?php
// FOOTER
require('layouts/footer.php')
// END FOOTER
?>

<!-- VIEW SCRIPT -->
<script src="../js/configuracion.js?ver=<?php echo VERSION?>"></script>

<?php
}
ob_end_flush();