<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: login');
} else {
    // HEADER
    require('layouts/header.php');
    // END HEADER?>

<!-- Container fluid -->
<div class="container-fluid" id="container-wrapper">

    <?php
        if ($_SESSION['v_clientes']==0) {
            echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
        } else { ?>

    <div class="justify-content-between mb-4">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-users"></i> Clientes</h1>
            </div>
            <div class="col-sm-4">
                <div class="btn-group float-right mt-3 mt-sm-0" role="group">
                    <?php
                        if ($_SESSION['new_clientes']==1) {
                            echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
                        } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div id="contenedor-cabecera"
                    class="card-header py-1 d-flex flex-row align-items-center justify-content-between">
                    <!--  -->
                </div>
                <!-- Tabla -->
                <div id="listado" class="table-responsive p-3">
                    <table id="tblListado" class="table align-items-center table-hover table-bordered"
                        style="width: 100%;">
                        <thead class="thead-light">
                            <th><i class="fas fa-th-list ml-2 mr-2"></i></th>
                            <th>&nbsp;Cod.&nbsp;</th>
                            <th>&nbsp;Estado&nbsp;</th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Cuit&nbsp;</th>
                            <th>&nbsp;Dni&nbsp;</th>
                            <th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Localidad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Provincia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>

                        </thead>
                    </table>
                </div>
                <!-- End tabla -->

                <!-- Formulario -->
                <div id="formulario">
                    <div class="card-body">
                        <form id="form">

                            <input type="hidden" id="codCliente" name="codCliente">

                            <div class="form-group row" id="divCodCliente">
                                <div class="col-12">
                                    <h5><span class="badge badge-secondary" id="codClienteText"></span></h5>
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label><span class="text-danger">(*)</span> Estado</label>
                                    <select id="estado" name="estado" data-lang="es_ES" title="Seleccione estado"
                                        class="selectpicker form-control" required>
                                        <option value="ACTIVO">ACTIVO</option>
                                        <option value="INACTIVO">INACTIVO</option>
                                    </select>
                                </div>

                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label><span class="text-danger">(*)</span> Cuit</label>
                                    <input type="text" class="form-control" id="cuit" name="cuit" data-maxsize="11"
                                        required autocomplete="off">
                                </div>

                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label><span class="text-danger">(*)</span> Apellido y Nombre</label>
                                    <input type="text" class="form-control" id="apellidoNombre" name="apellidoNombre"
                                        data-maxsize="30" required autocomplete="off"
                                        style="text-transform: capitalize;">
                                </div>

                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label><span class="text-danger">(*)</span> Dni</label>
                                    <input type="text" class="form-control" id="dni" name="dni" data-maxsize="8"
                                        required autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label><span class="text-danger">(*)</span> Domicilio</label>
                                    <input type="text" class="form-control" id="domicilio" name="domicilio"
                                        data-maxsize="60" required autocomplete="off"
                                        style="text-transform: capitalize;">
                                </div>

                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label><span class="text-danger">(*)</span> Telefono</label>
                                    <input type="text" class="form-control" id="telefono" name="telefono"
                                        data-maxsize="50" required autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label><span class="text-danger">(*)</span> Localidad</label>
                                    <input type="text" class="form-control" id="localidad" name="localidad"
                                        data-maxsize="50" required autocomplete="off"
                                        style="text-transform: capitalize;">
                                </div>

                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label><span class="text-danger">(*)</span> Provincia</label>
                                    <input type="text" class="form-control" id="provincia" name="provincia"
                                        data-maxsize="50" required autocomplete="off"
                                        style="text-transform: capitalize;">
                                </div>
                            </div>





                            <div class="mt-4 mb-4 float-right">
                                <button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
                                <button type="submit" id="btnGuardar" class="btn btn-primary ml-3">Guardar</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- End formulario -->
            </div>
        </div>
    </div>

    <?php } ?>

</div>
<!-- End Container fluid -->
</div>
<!-- End Content -->

<?php
// FOOTER
require('layouts/footer.php')
// END FOOTER
?>

<!-- VIEW SCRIPT -->
<script src="../js/cliente.js?ver=<?php echo VERSION?>"></script>

<?php
}
ob_end_flush();