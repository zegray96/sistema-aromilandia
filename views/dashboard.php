<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: login');
} else {
    // HEADER
    require('layouts/header.php');
    // END HEADER?>

<!-- Container fluid -->
<div class="container-fluid" id="container-wrapper">

    <div class="row mb-3">

        <?php if ($_SESSION['v_clientes']==1) { ?>

        <div class="col-xl-3 col-md-6 mb-4">
            <a class="a-dashboard" href="clientes">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col mr-2">
                                <div class="h5 mb-0 font-weight-bold text-gray-800">Clientes</div>
                                <div class="mt-2 mb-0 text-muted h5">
                                    <span id="cantClientesDashboard" class="text-success mr-2"></span>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-users fa-2x text-info"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <?php } ?>

        <?php if ($_SESSION['v_articulos']==1) { ?>

        <div class="col-xl-3 col-md-6 mb-4">
            <a class="a-dashboard" href="articulos">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col mr-2">
                                <div class="h5 mb-0 font-weight-bold text-gray-800">Articulos</div>
                                <div class="mt-2 mb-0 text-muted h5">
                                    <span id="cantClientesDashboard" class="text-success mr-2"></span>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-box-open fa-2x text-info"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <?php } ?>

        <?php if ($_SESSION['v_presupuestos']==1) { ?>

        <div class="col-xl-3 col-md-6 mb-4">
            <a class="a-dashboard" href="presupuestos">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col mr-2">
                                <div class="h5 mb-0 font-weight-bold text-gray-800">Presupuestos</div>
                                <div class="mt-2 mb-0 text-muted h5">
                                    <span id="cantClientesDashboard" class="text-success mr-2"></span>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-dollar-sign fa-2x text-info"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <?php } ?>

    </div>

    <div class="row mb-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Total de ventas <strong class="text-success"
                                id="totalPresupuestos">$0,00</strong></h1>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-6" id="divFiltrarPorFechas">
                            <div class="input-daterange input-group">
                                <input autocomplete="off" type="text" class="input-sm form-control"
                                    name="fechaIniFiltrar" id="fechaIniFiltrar" placeholder="Desde">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">-</span>
                                </div>
                                <input autocomplete="off" type="text" class="input-sm form-control"
                                    name="fechaFinFiltrar" id="fechaFinFiltrar" placeholder="Hasta">
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-6 mt-3 mt-md-0">
                            <div class="input-group">
                                <select name="idSucursal" id="idSucursal" data-lang="es_ES"
                                    class="selectpicker form-control pr-4" required>

                                </select>
                                <button type="button" id="btnCalcularTotalVentas" class="btn btn-primary"><i
                                        class="fas fa-sync-alt"></i></button>
                            </div>



                        </div>


                    </div>

                </div>

                <div id="listado" class="table-responsive p-3">
                    <table id="tblListado" class="table align-items-center table-hover table-bordered"
                        style="width: 100%;">
                        <thead class="thead-light">
                            <th>&nbsp;Estado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Hora&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;N°&nbsp;</th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Monto&nbsp;Total&nbsp;</th>
                            <th>&nbsp;Metodos&nbsp;de&nbsp;Pago&nbsp;y&nbsp;Descuentos&nbsp;
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>



</div>
<!-- End container fluid -->
</div>
<!-- End content -->

<?php
// FOOTER
require('layouts/footer.php')
// END FOOTER
?>

<!-- VIEW SCRIPT -->
<script src="../js/dashboard.js?ver=<?php echo VERSION?>"></script>

<?php
}
ob_end_flush();