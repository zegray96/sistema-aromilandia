<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: login');
} else {
    // HEADER
    require('layouts/header.php');
    // END HEADER?>

<!-- Container fluid -->
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-user"></i> Mi Cuenta</h1>
    </div>

    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <!-- Formulario -->
                <div id="formulario">
                    <div class="card-body">
                        <form id="form">

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label><span class="text-danger">(*)</span> Apellido y Nombre</label>
                                    <input style="text-transform: capitalize;" autocomplete="off" type="text"
                                        class="form-control" id="apellidoNombre" name="apellidoNombre" data-maxsize="30"
                                        required value="<?php echo $_SESSION['apellidoNombre']; ?>">
                                </div>

                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label><span class="text-danger">(*)</span> Usuario</label>
                                    <input autocomplete="off" type="text" class="form-control" id="usuario"
                                        name="usuario" data-maxsize="30" required
                                        value="<?php echo $_SESSION['usuario']; ?>">
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label class="mr-1"><span class="text-danger">(*)</span> Clave</label>
                                    <a id="linkModificarClave" data-toggle="modal" href="#modificarClave"
                                        class="badge badge-info" style="font-size: 15px"> Modificar</a>
                                    <input type="password" class="form-control" id="clave" name="clave"
                                        data-maxsize="100" required placeholder="Ingrese clave" value="*******"
                                        readonly>
                                </div>
                            </div>



                            <div class="mt-4 mb-4 float-right">
                                <button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
                                <button type="submit" id="btnGuardar" class="btn btn-primary ml-3">Guardar</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- End formulario -->
            </div>
        </div>
    </div>

    <!-- Modales -->

    <!-- Modificar clave -->
    <div class="modal fade" id="modificarClave" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
        data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="formModificarClave">

                        <div class="form-group">
                            <input type="password" class="form-control" id="claveModificar" name="claveModificar"
                                data-maxsize="100" required placeholder="Ingrese nueva clave">
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control" id="repetirClaveModificar"
                                name="repetirClaveModificar" data-maxsize="100" required
                                placeholder="Repita nueva clave">
                        </div>

                        <div class="mb-4 float-right">
                            <button type="button" id="btnCancelarModificarClave"
                                class="btn btn-danger">Cancelar</button>
                            <button type="submit" id="btnGuardarModificarClave" class="btn btn-primary">Guardar</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- Fin modificar clave -->

    <!-- Fin modales -->
</div>
<!-- End Container fluid -->
</div>
<!-- End Content -->

<?php
// FOOTER
require('layouts/footer.php')
// END FOOTER
?>

<!-- VIEW SCRIPT -->
<script src="../js/miCuenta.js?ver=<?php echo VERSION?>"></script>

<?php
}
ob_end_flush();