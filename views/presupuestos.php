<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: login');
} else {
    // HEADER
    require('layouts/header.php');
    // END HEADER?>

<!-- Container fluid -->
<div class="container-fluid" id="container-wrapper">

    <?php
    if ($_SESSION['v_articulos'] == 0) {
        echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
    } else { ?>

    <div class="justify-content-between mb-4">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-dollar-sign"></i> Presupuestos</h1>

            </div>
            <div class="col-sm-6">


                <div class="float-right mt-2 mt-sm-0 ml-2" role="group">

                    <?php
                        if ($_SESSION['new_presupuestos'] == 1) {
                            echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
                        } ?>
                </div>

                <div id="divFiltrar" class="dropdown float-right mt-2 mt-sm-0">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-filter"></i> Filtrar
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" data-toggle="modal" href="#filtrarPorFecha">Por Fecha</a>
                        <a class="dropdown-item" data-toggle="modal" href="#filtrarPorCliente">Por Cliente</a>
                        <a class="dropdown-item" data-toggle="modal" href="#filtrarPorSucursal">Por Sucursal</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div id="contenedor-cabecera"
                    class="card-header pt-3 px-3 d-flex flex-row align-items-center justify-content-between">

                    <h4><span class="badge badge-blue">Mostrando registros de <span
                                id="textSucursalMostrando"></span></span></h4>

                </div>
                <!-- Tabla -->
                <div id="listado" class="table-responsive p-3">
                    <table id="tblListado" class="table align-items-center table-hover table-bordered"
                        style="width: 100%;">
                        <thead class="thead-light">
                            <th><i class="fas fa-th-list ml-5 pr-5"></i></th>
                            <th>&nbsp;Estado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Fecha&nbsp;Presupuesto&nbsp;</th>
                            <th>&nbsp;Hora&nbsp;Presupuesto&nbsp;</th>
                            <th>&nbsp;N°&nbsp;Presupuesto&nbsp;</th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Dni&nbsp;
                            </th>
                            <th>&nbsp;Monto&nbsp;Total&nbsp;
                            </th>
                            <th>&nbsp;Metodos&nbsp;de&nbsp;Pago&nbsp;y&nbsp;Descuentos&nbsp;
                            </th>
                            <th>&nbsp;Sucursal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                        </thead>
                    </table>
                </div>
                <!-- End tabla -->

                <!-- Formulario -->
                <div id="formulario">
                    <div class="card-body">
                        <form id="form">


                            <input type="hidden" id="idPresupuesto" name="idPresupuesto">



                            <!-- Encabezado presupuesto -->
                            <div class="form-group row mb-4">
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <label>N° Presupuesto</label>
                                    <input type="text" class="form-control" id="nroPresupuesto" name="nroPresupuesto"
                                        readonly>
                                </div>


                                <div class="col-12 col-sm-6 col-lg-3 mt-3 mt-sm-0">
                                    <label><span class="text-danger">(*)</span> Estado</label>
                                    <select name="estadoPresu" id="estadoPresu" data-lang="es_ES"
                                        class="selectpicker form-control" required>
                                        <option value="PAGADO">PAGADO</option>
                                        <option value="FALTA PAGAR">FALTA PAGAR</option>
                                    </select>
                                </div>

                                <div class="d-none d-lg-block col-lg-1">

                                </div>

                                <div class="col-12 col-sm-6 col-lg-3 mt-3 mt-lg-0">
                                    <label><span class="text-danger">(*)</span> Fecha Presup.</label>
                                    <div class="input-group date">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                                        </div>
                                        <input type="text" class="form-control" id="fechaPresupuesto"
                                            name="fechaPresupuesto" required autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-lg-2 mt-3 mt-lg-0">
                                    <label>Hora</label>
                                    <input type="text" class="form-control" id="horaPresupuesto" name="horaPresupuesto"
                                        readonly>
                                </div>
                            </div>
                            <hr>
                            <!-- Fin encabezado presupuesto -->

                            <!-- Datos cliente -->
                            <div class="form-group row mb-4">
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <label><span class="text-danger">(*)</span> Cod. Cliente</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="codCliente" name="codCliente"
                                            autocomplete="off">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-primary" id="btnBuscarCliente"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-success" id="btnNuevoCliente"><i
                                                    class="fas fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-lg-3 mt-3 mt-sm-0">
                                    <label>Dni</label>
                                    <input type="text" class="form-control" id="dniPresu" name="dniPresu" readonly>
                                </div>

                                <div class="col-12 col-lg-6 mt-3 mt-lg-0">
                                    <label>Apellido y Nombre</label>
                                    <input type="text" class="form-control" id="apellidoNombrePresu"
                                        name="apellidoNombrePresu" readonly>
                                </div>

                                <div class="col-12 col-sm-6 col-lg-3 mt-3">
                                    <label>Cuit</label>
                                    <input type="text" class="form-control" id="cuitPresu" name="cuitPresu" readonly>
                                </div>

                            </div>

                            <hr>
                            <!-- Fin datos cliente  -->

                            <!-- Datos articulo -->
                            <div class="divDatosArticuloConImg">
                                <div class="mt-3 mb-3 imgDatosArticulo">
                                    <img id="imagenImg" src="../imgArticulos/sin-imagen.jpg" class="thumbnail-imagen"
                                        width="150px" height="150px">
                                </div>
                                <div class="row mb-4" style="padding:0; margin:0; width:100%;">
                                    <div class="col-lg-4 p-0 pr-lg-4 pl-lg-4">
                                        <label><span class="text-danger">(*)</span> Cod. Articulo</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="codArticulo" name="codArticulo"
                                                autocomplete="off">
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-primary" id="btnBuscarArticulo"><i
                                                        class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 mt-3 mt-lg-0 p-0">
                                        <label><span class="text-danger">(*)</span> Descripción</label>
                                        <input type="text" class="form-control" id="descripcion" name="descripcion"
                                            autocomplete="off" readonly style="text-transform: capitalize;">
                                    </div>

                                    <div class="col-lg-4 mt-3 p-0 pr-lg-4 pl-lg-4">
                                        <label><span class="text-danger">(*)</span> Precio Unit.</label>
                                        <input type="text" class="form-control" id="precioUnitario"
                                            name="precioUnitario" placeholder="00,0" autocomplete="off" readonly>
                                    </div>

                                    <div class="col-lg-5 mt-3 p-0">
                                        <label><span class="text-danger">(*)</span> Cantidad</label>
                                        <div class="input-group">
                                            <input type="number" min="1" class="form-control" id="cantidad"
                                                name="cantidad">
                                            <div class="input-group-append">
                                                <button id="btnAgregarDetalle" type="button"
                                                    class="btn btn-success btn-agregar"><i class="fas fa-cart-plus"></i>
                                                    Agregar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- Fin datos articulo -->

                            <!-- Tabla -->
                            <div style="height: 60vh !important;" class="panel-body table-responsive">
                                <table id="tblDetalles"
                                    class="table table-striped table-bordered table-condensed table-hover"
                                    style="width:100%; ">
                                    <thead class="thead-dark">
                                        <th id="opDetalle"><i class="fas fa-th-list ml-2 mr-2"></i></th>
                                        <th>Descripción</th>
                                        <th>Cantidad</th>
                                        <th>Precio&nbsp;Unit.</th>
                                        <th>Subtotal</th>
                                    </thead>

                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                            <hr>
                            <!-- Fin tabla -->


                            <!-- Desc y metodos de pagos -->
                            <div class="form-group row mb-4">

                                <div class="col-12 col-sm-6 col-lg-3">
                                    <h5 class="font-weight-bold"> Tipo Descuento</h5>
                                    <select id="tipoDesc" name="tipoDesc" data-lang="es_ES"
                                        class="selectpicker form-control" required>
                                        <option value="sin_descuento" selected>Sin descuento</option>
                                        <option value="porcentaje">%</option>
                                        <option value="monto">$</option>
                                    </select>
                                </div>

                                <div class="col-12 col-sm-6 col-lg-3 mt-3 mt-sm-0">
                                    <h5 class="font-weight-bold">Valor Descuento</h5>
                                    <input type="number" min="0" class="font-weight-bold form-control" id="valorDesc"
                                        name="valorDesc">
                                </div>
                                <div class="col-12 col-lg-6 mt-3 mt-lg-0">
                                    <h5 class="font-weight-bold"> Metodos de pago</h5>

                                    <div class="panel-body table-responsive">
                                        <table id="tblMetodosPago"
                                            class="table table-striped table-bordered table-condensed table-hover"
                                            style="width:100%; ">
                                            <thead class="thead-dark">
                                                <th id="opMetodoPago"><button onclick="abrirAgregarMetodoPago()"
                                                        type="button" class="btn btn-success btn-sm"><i
                                                            class="fas fa-plus-circle"></i></button></th>
                                                <th>Metodo&nbsp;de&nbsp;Pago</th>
                                                <th>Monto</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <!-- Fin Desc y metodos de pagos -->

                            <!-- Total -->
                            <div class="form-group row mb-4">
                                <div class="col-sm-6 col-lg-8">
                                </div>
                                <div class="col-12 col-sm-6 col-lg-4 mt-3 mt-lg-0">
                                    <h5 class="font-weight-bold"> Total</h5>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" class="font-weight-bold form-control" id="montoTotal"
                                            name="montoTotal" readonly>
                                    </div>
                                </div>
                            </div>
                            <!-- Fin total -->

                            <div class="mt-4 mb-4 float-right">
                                <button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
                                <button type="submit" id="btnGuardar" class="btn btn-primary ml-3">Guardar</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- End formulario -->
            </div>
        </div>
    </div>

    <!-- Modales -->

    <!-- nuevo cliente-->
    <div class="modal fade" id="nuevoCliente" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
        data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body table-responsive">
                    <form id="formNuevoCliente">

                        <div class="form-group row mb-3">
                            <div class="col-12 col-md-6">
                                <label><span class="text-danger">(*)</span> Estado</label>
                                <select id="estado" name="estado" data-lang="es_ES" title="Seleccione estado"
                                    class="selectpicker form-control" required>
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="INACTIVO">INACTIVO</option>
                                </select>
                            </div>

                            <div class="col-12 col-md-6 mt-3 mt-md-0">
                                <label><span class="text-danger">(*)</span> Cuit</label>
                                <input type="text" class="form-control" id="cuit" name="cuit" data-maxsize="11" required
                                    autocomplete="off">
                            </div>

                        </div>

                        <div class="form-group row mb-3">
                            <div class="col-12 col-md-6">
                                <label><span class="text-danger">(*)</span> Apellido y Nombre</label>
                                <input type="text" class="form-control" id="apellidoNombre" name="apellidoNombre"
                                    data-maxsize="30" required autocomplete="off" style="text-transform: capitalize;">
                            </div>

                            <div class="col-12 col-md-6 mt-3 mt-md-0">
                                <label><span class="text-danger">(*)</span> Dni</label>
                                <input type="text" class="form-control" id="dni" name="dni" data-maxsize="8" required
                                    autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <div class="col-12 col-md-6">
                                <label><span class="text-danger">(*)</span> Domicilio</label>
                                <input type="text" class="form-control" id="domicilio" name="domicilio"
                                    data-maxsize="60" required autocomplete="off" style="text-transform: capitalize;">
                            </div>

                            <div class="col-12 col-md-6 mt-3 mt-md-0">
                                <label><span class="text-danger">(*)</span> Telefono</label>
                                <input type="text" class="form-control" id="telefono" name="telefono" data-maxsize="50"
                                    required autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <div class="col-12 col-md-6">
                                <label><span class="text-danger">(*)</span> Localidad</label>
                                <input type="text" class="form-control" id="localidad" name="localidad"
                                    data-maxsize="50" required autocomplete="off" style="text-transform: capitalize;">
                            </div>

                            <div class="col-12 col-md-6 mt-3 mt-md-0">
                                <label><span class="text-danger">(*)</span> Provincia</label>
                                <input type="text" class="form-control" id="provincia" name="provincia"
                                    data-maxsize="50" required autocomplete="off" style="text-transform: capitalize;">
                            </div>
                        </div>


                        <div class="mt-4 mb-4 float-right">
                            <button type="button" id="btnCancelarCliente" class="btn btn-danger">Cancelar</button>
                            <button type="submit" id="btnGuardarCliente" class="btn btn-primary ml-3">Guardar</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin nuevo cliente -->

    <!-- Buscar cliente  para presupuesto-->
    <div class="modal fade" id="buscarClienteParaPresu" tabindex="-1" role="dialog" aria-hidden="true"
        data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body table-responsive">
                    <div class="form-group row mb-3">
                        <div class="col-12 col-md-6">
                            <input type="text" class="form-control" id="buscarNombreEnPresu"
                                placeholder="Buscar apellido y nombre" autocomplete="off">
                        </div>

                        <div class="col-12 col-md-6 mt-3 mt-md-0">
                            <input type="text" class="form-control" id="buscarDniEnPresu" placeholder="Buscar dni"
                                autocomplete="off">
                        </div>
                    </div>

                    <table id="tblClientesPresu" class="table table-striped table-bordered table-condensed table-hover"
                        style="width: 100%;">
                        <thead>
                            <th><i class="fas fa-th-list ml-2 mr-2"></i></th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Cuit&nbsp;</th>
                            <th>&nbsp;Dni&nbsp;</th>
                            <th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Localidad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Provincia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin buscar cliente para presupuesto -->


    <!-- Buscar articulo-->
    <div class="modal fade" id="buscarArticulo" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
        data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body table-responsive">
                    <div class="form-group row mb-3">
                        <div class="col-12 col-md-6">
                            <input type="text" class="form-control" id="buscarDescripcionEnPresu"
                                placeholder="Buscar descripción" autocomplete="off">
                        </div>

                        <div class="col-12 col-md-6 mt-3 mt-md-0">
                            <input type="text" class="form-control" id="buscarCodArtEnPresu" placeholder="Buscar codigo"
                                autocomplete="off">
                        </div>
                    </div>

                    <table id="tblArticulos" class="table table-striped table-bordered table-condensed table-hover"
                        style="width: 100%;">
                        <thead>
                            <th><i class="fas fa-th-list ml-2 mr-2"></i></th>
                            <th>&nbsp;Cod.&nbsp;Art.&nbsp;</th>
                            <th>&nbsp;Descripción&nbsp;</th>
                            <th>&nbsp;Imagen&nbsp;</th>
                            <th>&nbsp;Stock&nbsp;Actual&nbsp;</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin buscar articulo -->

    <!-- Listar presupuestos por cliente-->
    <div class="modal fade" id="listarPresupuestosPorCliente" tabindex="-1" role="dialog" aria-hidden="true"
        data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body table-responsive">
                    <button type="button" class="btn btn-success mb-3" id="btnAceptarPresupuesto"> Aceptar</button>
                    <table id="tblPresupuestosPorCliente"
                        class="table table-striped table-bordered table-condensed table-hover" style="width: 100%;">
                        <thead>
                            <th><i class="fas fa-th-list ml-2 mr-2"></i></th>
                            <th>&nbsp;Estado&nbsp;</th>
                            <th>&nbsp;Fecha&nbsp;Presup.&nbsp;</th>
                            <th>&nbsp;N°&nbsp;Presupuesto&nbsp;</th>
                            <th>&nbsp;Monto&nbsp;Total&nbsp;</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin listar presupuestos por cliente -->

    <!-- Filtrar por fecha -->
    <div class="modal fade" id="filtrarPorFecha" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
        data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group" id="divFiltrarPorFechas">
                        <div class="input-daterange input-group">
                            <input autocomplete="off" type="text" class="input-sm form-control" name="fechaIniFiltrar"
                                id="fechaIniFiltrar" placeholder="Desde">
                            <div class="input-group-prepend">
                                <span class="input-group-text">-</span>
                            </div>
                            <input autocomplete="off" type="text" class="input-sm form-control" name="fechaFinFiltrar"
                                id="fechaFinFiltrar" placeholder="Hasta">
                        </div>
                    </div>

                    <div class="float-right">
                        <button type="button" id="btnFiltrarPorFechas" class="btn btn-primary">Filtrar</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Fin filtrar fecha -->

    <!-- Filtrar por cliente-->
    <div class="modal fade" id="filtrarPorCliente" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
        data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body table-responsive">
                    <div class="form-group row mb-3">
                        <div class="col-12 col-md-6">
                            <input type="text" class="form-control" id="buscarNombreEnFiltrar"
                                placeholder="Buscar apellido y nombre" autocomplete="off">
                        </div>

                        <div class="col-12 col-md-6 mt-3 mt-md-0">
                            <input type="text" class="form-control" id="buscarDniEnFiltrar" placeholder="Buscar dni"
                                autocomplete="off">
                        </div>
                    </div>

                    <table id="tblFiltrarPorCliente"
                        class="table table-striped table-bordered table-condensed table-hover" style="width: 100%;">
                        <thead>
                            <th><i class="fas fa-th-list ml-2 mr-2"></i></th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Cuit&nbsp;</th>
                            <th>&nbsp;Dni&nbsp;</th>
                            <th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Localidad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Provincia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin fitrar por cliente -->

    <!-- Filtrar por sucursal-->
    <div class="modal fade" id="filtrarPorSucursal" tabindex="-1" role="dialog" aria-hidden="true"
        data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="col-12" id="divFiltrarPorFechas">
                        <div class="input-daterange input-group">
                            <input autocomplete="off" type="text" class="input-sm form-control"
                                name="fechaIniFiltrarPorSucursal" id="fechaIniFiltrarPorSucursal" placeholder="Desde">
                            <div class="input-group-prepend">
                                <span class="input-group-text">-</span>
                            </div>
                            <input autocomplete="off" type="text" class="input-sm form-control"
                                name="fechaFinFiltrarPorSucursal" id="fechaFinFiltrarPorSucursal" placeholder="Hasta">
                        </div>
                    </div>

                    <div class="col-12 mt-3">
                        <select name="idSucursalFiltrarPorSucursal" id="idSucursalFiltrarPorSucursal" data-lang="es_ES"
                            class="selectpicker form-control" required>

                        </select>
                    </div>
                    <div class="col-12">
                        <div class="float-right mt-3">
                            <button type="button" id="btnFiltrarPorSucursal" class="btn btn-primary">Filtrar</button>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- Fin filtrar por sucursal -->

    <!-- Agregar metodo de pago -->
    <div class="modal fade" id="agregarMetodoPago" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
        data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Monto restante para llegar al total <strong id="montoMetodoPagoRestante">$1200,00</strong></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body table-responsive">
                    <div class="form-group row mb-3">
                        <div class="col-12 col-md-6">
                            <label><span class="text-danger">(*)</span> Metodo de Pago</label>
                            <select id="idMetodoPago" name="idMetodoPago" data-lang="es_ES"
                                title="Seleccione metodo de pago" class="selectpicker form-control" required>

                            </select>
                        </div>

                        <div class="col-12 col-md-6 mt-3 mt-md-0">
                            <label><span class="text-danger">(*)</span> Monto</label>
                            <input type="text" class="form-control" id="montoMetodoPago" name="montoMetodoPago" required
                                autocomplete="off">
                        </div>

                    </div>

                    <div class="mt-4 mb-4 float-right">
                        <button type="button" id="btnCancelarMetodoPago" class="btn btn-danger">Cancelar</button>
                        <button type="button" id="btnAgregarMetodoPago" class="btn btn-primary ml-3">Agregar</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Fin agregar metodo de pago -->

    <!-- Fin modales -->

    <?php } ?>

</div>
<!-- End Container fluid -->
</div>
<!-- End Content -->

<?php
// FOOTER
require('layouts/footer.php')
// END FOOTER
?>

<!-- VIEW SCRIPT -->
<script src="../js/presupuesto.js?ver=<?php echo VERSION ?>"></script>

<?php
}
ob_end_flush();