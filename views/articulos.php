<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: login');
} else {
    // HEADER
    require('layouts/header.php');
    // END HEADER?>

<!-- Container fluid -->
<div class="container-fluid" id="container-wrapper">

    <?php
            if ($_SESSION['v_articulos'] == 0) {
                echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
                        </div>';
            } else { ?>

    <div class="justify-content-between mb-4">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-box-open"></i> Articulos</h1>
            </div>
            <div class="col-xl-4">
                <div class="btn-group float-right mt-2 mt-sm-0 ml-2" role="group">

                    <?php
                        if ($_SESSION['new_articulos'] == 1) {
                            echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
                        } ?>
                </div>

                <div class="dropdown float-right mt-2 mt-sm-0">
                    <button type="button" id='btnCatalogoPrecios' class='btn btn-primary'><i
                            class="fas fa-clipboard-list"></i>
                        Catalogo de
                        Precios</button>
                </div>
            </div>
        </div>
    </div>


    <div class="row mb-3">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div id="contenedor-cabecera"
                    class="card-header py-1 d-flex flex-row align-items-center justify-content-between">
                    <!--  -->
                </div>
                <!-- Tabla -->
                <div id="listado" class="table-responsive p-3">
                    <table id="tblListado" class="table align-items-center table-hover table-bordered"
                        style="width: 100%;">
                        <thead class="thead-light">
                            <th><i class="fas fa-th-list ml-2 mr-2"></i></th>
                            <th>&nbsp;Cod.&nbsp;</th>
                            <th>&nbsp;Estado&nbsp;</th>
                            <th>&nbsp;Descripción&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Categoria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </th>
                            <th>&nbsp;Imagen&nbsp;</th>
                            <th>&nbsp;Precio&nbsp;U.&nbsp;Mayorista&nbsp;</th>
                            <th>&nbsp;Precio&nbsp;U.&nbsp;Público&nbsp;</th>
                            <th>&nbsp;Stock&nbsp;Actual&nbsp;</th>
                        </thead>
                    </table>
                </div>
                <!-- End tabla -->

                <!-- Formulario -->
                <div id="formulario">
                    <div class="card-body">
                        <form id="form">

                            <input type="hidden" id="codArticulo" name="codArticulo">

                            <div class="form-group row" id="divCodArticulo">
                                <div class="col-12">
                                    <h5><span class="badge badge-secondary" id="codArticuloText"></span></h5>
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-md-12 col-lg-4">
                                    <label><span class="text-danger">(*)</span> Descripción</label>
                                    <input type="text" class="form-control" id="descripcion" name="descripcion"
                                        autocomplete="off" data-maxsize="55" required
                                        style="text-transform: capitalize;">
                                </div>

                                <div class="col-12 col-md-6 col-lg-4 mt-3 mt-lg-0">
                                    <label>Precio U. Mayorista</label>
                                    <input type="text" class="form-control" id="precioUnitarioMayorista"
                                        name="precioUnitarioMayorista" placeholder="00.0" autocomplete="off">
                                </div>

                                <div class="col-12 col-md-6 col-lg-4 mt-3 mt-lg-0">
                                    <label>Precio U. Público</label>
                                    <input type="text" class="form-control" id="precioUnitarioPublico"
                                        name="precioUnitarioPublico" placeholder="00.0" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6">
                                    <label>Stock Actual</label>
                                    <input type="number" min="1" class="form-control" id="stockActual"
                                        name="stockActual" readonly>
                                </div>

                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label>Cantidad</label>
                                    <div class="input-group">
                                        <input type="number" min="1" class="form-control" id="cantidad" name="cantidad">
                                        <div class="input-group-append">
                                            <button type="button" id="sumarStock" class="btn btn-success"><i
                                                    class="fas fa-plus"></i></button>
                                            <button type="button" id="restarStock" class="btn btn-danger"><i
                                                    class="fas fa-minus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label>Imagen <span class="text-danger">(.jpg .png)</span></label>
                                    <input type="file" class="form-control" name="imagen" id="imagen">

                                    <div class="mt-3 div-img-logo-icono">
                                        <img id="imagenImg" class="thumbnail-imagen" width="150px" height="150px">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label><span class="text-danger">(*)</span> Categoria</label>
                                    <select id="idCategoria" name="idCategoria" data-lang="es_ES"
                                        title="Seleccione categoria" class="selectpicker form-control" required
                                        data-live-search="true">

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <div class="col-12 col-md-6 mt-3 mt-md-0">
                                    <label><span class="text-danger">(*)</span> Estado</label>
                                    <select id="estado" name="estado" data-lang="es_ES" title="Seleccione estado"
                                        class="selectpicker form-control" required>
                                        <option value="ACTIVO">ACTIVO</option>
                                        <option value="INACTIVO">INACTIVO</option>
                                    </select>
                                </div>
                            </div>


                            <div class="mt-4 mb-4 float-right">
                                <button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
                                <button type="submit" id="btnGuardar" class="btn btn-primary ml-3">Guardar</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- End formulario -->
            </div>
        </div>
    </div>

    <?php } ?>

</div>
<!-- End Container fluid -->
</div>
<!-- End Content -->

<?php
// FOOTER
require('layouts/footer.php')
// END FOOTER
?>

<!-- VIEW SCRIPT -->
<script src="../js/articulo.js?ver=<?php echo VERSION ?>"></script>

<?php
}
ob_end_flush();