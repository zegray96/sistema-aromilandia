var tabla;
function init() {
	$('#tituloPagina').text('Sistema Aromilandia - Roles');
	listar();
	mostrar_form(false);
	$("#wrapper").show();
	$("#preload").hide();

}

// ajustamos las columnas de tabla
$('#sidebarToggleTop').click(function (e) {
	setTimeout(function () {
		tabla.columns.adjust();
	}, 300);
});

//evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach(node => node.addEventListener('keypress', e => {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	}))

	document.querySelectorAll('input[type=number]').forEach(node => node.addEventListener('keypress', e => {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	}))

	document.querySelectorAll('input[type=password]').forEach(node => node.addEventListener('keypress', e => {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	}))
});
// fin enter verificacion

$('#btnNuevo').click(function () {
	mostrar_form(true);
	$('#nombre').focus();
});

$('#btnCancelar').click(function () {
	mostrar_form(false);
});

$("#form").on("submit", function (e) {
	nuevo_editar(e);
});

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
	return str.replace(/\w\S*/g, function (txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

$('#nombre').blur(function () {
	$('#nombre').val(capitalizar($('#nombre').val()));
});

function listar() {
	tabla = $('#tblListado').dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
		"language": {
			"url": "../assets/json/Spanish.json"
		},
		"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons: [
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',

			},


			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',

			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',

			},



		],

		"ajax": {
			url: '../ajax/rol.php?op=listar',
			type: 'get',
			dataType: 'json',
			error: function (e) {
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength": 25, //cada cuantos registros realizamos la paginacion
		"order": [[1, "asc"]] //para ordenar los registros
	}).DataTable();
}


function mostrar_form(mostrar) {
	limpiar();
	if (mostrar) {
		$('#listado').hide();
		$('#formulario').show();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
		$('#btnNuevo').css("cssText", "display:none !important");
		$('#btnGuardar').prop("disabled", false);
	} else {
		$('#listado').show();
		$('#formulario').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");
		$('#btnNuevo').css("cssText", "display:block !important");

	}
}

function limpiar() {
	$('#idRol').val("");
	$('#nombre').val("");
	$('#vConfiguracion').prop("checked", false);
	$('#vAcceso').prop("checked", false);
	$('#vClientes').prop("checked", false);
	$('#vArticulos').prop("checked", false);
	$('#vMetodosPago').prop("checked", false);
	$('#vCategoriasArt').prop("checked", false);
	$('#vPresupuestos').prop("checked", false);
	$('#newAcceso').prop("checked", false);
	$('#newClientes').prop("checked", false);
	$('#newArticulos').prop("checked", false);
	$('#newCategoriasArt').prop("checked", false);
	$('#newPresupuestos').prop("checked", false);
	$('#newMetodosPago').prop("checked", false);
	$('#altAcceso').prop("checked", false);
	$('#altClientes').prop("checked", false);
	$('#altArticulos').prop("checked", false);
	$('#altCategoriasArt').prop("checked", false);
	$('#altPresupuestos').prop("checked", false);
	$('#altMetodosPago').prop("checked", false);
	$('#delCategoriasArt').prop("checked", false);
	$('#delPresupuestos').prop("checked", false);
	$('#delMetodosPago').prop("checked", false);
}

function nuevo_editar(e) {
	$('#cargandoModal').modal('show');

	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url: '../ajax/rol.php?op=nuevo_editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType: false,
		processData: false,

		success: function (datos) {
			$('#cargandoModal').fadeOut(300, function () {
				$('#cargandoModal').modal('hide');
			});

			if (datos == "¡Registro creado con exito!" || datos == "¡Registro editado con exito!") {
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				}).then(function () {
					$('#nombre').focus();
				});
				limpiar();
				tabla.ajax.reload();
			} else {
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}
	});
}

function mostrar(id) {
	$('#cargandoModal').modal('show');
	$.post("../ajax/rol.php?op=mostrar", { idRol: id }, function (data, status) {
		try {
			/* Si el JSON está mal formado se generará una excepción */
			data = JSON.parse(data);
			if (data.error == true) {
				/* Si hemos enviado por JSON un error, lo notificamos */
				console.log('ERROR detectado:', data);
				return;
			}
			/* Trabajamos habitualmente con la respuesta */
			$('#cargandoModal').fadeOut(300, function () {
				$('#cargandoModal').modal('hide');
			});
			mostrar_form(true);
			$('#idRol').val(data.id_rol);
			$('#nombre').val(data.nombre);

			if (data.v_acceso == 1) {
				$('#vAcceso').prop('checked', true);
			} else {
				$('#vAcceso').prop('checked', false);
			}

			if (data.v_clientes == 1) {
				$('#vClientes').prop('checked', true);
			} else {
				$('#vClientes').prop('checked', false);
			}

			if (data.v_articulos == 1) {
				$('#vArticulos').prop('checked', true);
			} else {
				$('#vArticulos').prop('checked', false);
			}

			if (data.v_categorias_art == 1) {
				$('#vCategoriasArt').prop('checked', true);
			} else {
				$('#vCategoriasArt').prop('checked', false);
			}

			if (data.v_presupuestos == 1) {
				$('#vPresupuestos').prop('checked', true);
			} else {
				$('#vPresupuestos').prop('checked', false);
			}

			if (data.v_configuracion == 1) {
				$('#vConfiguracion').prop('checked', true);
			} else {
				$('#vConfiguracion').prop('checked', false);
			}

			if (data.v_metodos_pago == 1) {
				$('#vMetodosPago').prop('checked', true);
			} else {
				$('#vMetodosPago').prop('checked', false);
			}

			if (data.new_acceso == 1) {
				$('#newAcceso').prop('checked', true);
			} else {
				$('#newAcceso').prop('checked', false);
			}

			if (data.new_clientes == 1) {
				$('#newClientes').prop('checked', true);
			} else {
				$('#newClientes').prop('checked', false);
			}

			if (data.new_articulos == 1) {
				$('#newArticulos').prop('checked', true);
			} else {
				$('#newArticulos').prop('checked', false);
			}

			if (data.new_categorias_art == 1) {
				$('#newCategoriasArt').prop('checked', true);
			} else {
				$('#newCategoriasArt').prop('checked', false);
			}

			if (data.new_presupuestos == 1) {
				$('#newPresupuestos').prop('checked', true);
			} else {
				$('#newPresupuestos').prop('checked', false);
			}

			if (data.new_metodos_pago == 1) {
				$('#newMetodosPago').prop('checked', true);
			} else {
				$('#newMetodosPago').prop('checked', false);
			}

			if (data.alt_acceso == 1) {
				$('#altAcceso').prop('checked', true);
			} else {
				$('#altAcceso').prop('checked', false);
			}

			if (data.alt_clientes == 1) {
				$('#altClientes').prop('checked', true);
			} else {
				$('#altClientes').prop('checked', false);
			}

			if (data.alt_articulos == 1) {
				$('#altArticulos').prop('checked', true);
			} else {
				$('#altArticulos').prop('checked', false);
			}

			if (data.alt_categorias_art == 1) {
				$('#altCategoriasArt').prop('checked', true);
			} else {
				$('#altCategoriasArt').prop('checked', false);
			}

			if (data.alt_presupuestos == 1) {
				$('#altPresupuestos').prop('checked', true);
			} else {
				$('#altPresupuestos').prop('checked', false);
			}

			if (data.alt_metodos_pago == 1) {
				$('#altMetodosPago').prop('checked', true);
			} else {
				$('#altMetodosPago').prop('checked', false);
			}

			if (data.del_presupuestos == 1) {
				$('#delPresupuestos').prop('checked', true);
			} else {
				$('#delPresupuestos').prop('checked', false);
			}

			if (data.del_categorias_art == 1) {
				$('#delCategoriasArt').prop('checked', true);
			} else {
				$('#delCategoriasArt').prop('checked', false);
			}

			if (data.del_metodos_pago == 1) {
				$('#delMetodosPago').prop('checked', true);
			} else {
				$('#delMetodosPago').prop('checked', false);
			}

		} catch (error) {
			/* Si el JSON está mal, notificamos su contenido */
			console.log('ERROR. Recibido:', data);
		}
	})
}



init();