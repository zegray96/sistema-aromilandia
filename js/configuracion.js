function init() {
	$('#tituloPagina').text('Sistema Aromilandia - Configuración');
	$("#wrapper").show();
	$("#preload").hide();
	mostrar();
}

$('#btnCancelar').click(function () {
	$(location).attr("href", "dashboard");
});


$("#form").on("submit", function (e) {
	editar(e);
});


//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
	return str.replace(/\w\S*/g, function (txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

// input formato moneda
$('#compraMinimaMayorista').keyup(function (event) {

	if (event.which >= 37 && event.which <= 40) {
		event.preventDefault();
	}

	$(this).val(function (index, value) {
		return value.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1,$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
	});

});

// input formato moneda
$('#siguientesComprasMayoristas').keyup(function (event) {

	if (event.which >= 37 && event.which <= 40) {
		event.preventDefault();
	}

	$(this).val(function (index, value) {
		return value.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1,$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
	});

});

$('#nombreEmpresa').blur(function () {
	$('#nombreEmpresa').val(capitalizar($('#nombreEmpresa').val()));
});


function validar_mostrar_logo(input) {
	var archivo = $("#logoEmpresa").val();
	var extensiones = archivo.substring(archivo.lastIndexOf("."));
	if (extensiones != ".png") {
		Swal.fire({
			icon: 'error',
			title: "El archivo de tipo " + extensiones + " no es válido",
		});
		$("#logoEmpresa").val(null);
	} else {
		var reader = new FileReader();
		reader.onload = function (e) {
			// Asignamos el atributo src a la tag de imagen
			$('#logoEmpresaImg').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}


$('#logoEmpresa').on('change', function () {
	validar_mostrar_logo(this);
});



function mostrar() {
	$('#cargandoModal').modal('show');
	$.post("../ajax/configuracion.php?op=mostrar", function (data, status) {
		try {
			/* Si el JSON está mal formado se generará una excepción */
			data = JSON.parse(data);
			if (data.error == true) {
				/* Si hemos enviado por JSON un error, lo notificamos */
				console.log('ERROR detectado:', data);
				return;
			}
			/* Trabajamos habitualmente con la respuesta */
			$('#cargandoModal').fadeOut(300, function () {
				$('#cargandoModal').modal('hide');
			});
			$('#nombreEmpresa').val(data.nombre_empresa);
			if (data.compra_minima_mayorista == 0) {
				$('#compraMinimaMayorista').val('');
			} else {
				$('#compraMinimaMayorista').val(Intl.NumberFormat("es", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data.compra_minima_mayorista));
			}
			if (data.siguientes_compras_mayoristas == 0) {
				$('#siguientesComprasMayoristas').val('');
			} else {
				$('#siguientesComprasMayoristas').val(Intl.NumberFormat("es", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data.siguientes_compras_mayoristas));
			}
			$('#logoEmpresaImg').attr("src", "../assets/img/logo-empresa.png" + "?" + Math.random());

		} catch (error) {
			/* Si el JSON está mal, notificamos su contenido */
			console.log('ERROR. Recibido:', data);
		}
	})
}

function editar(e) {
	$('#cargandoModal').modal('show');

	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url: '../ajax/configuracion.php?op=editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType: false,
		processData: false,

		success: function (datos) {
			$('#cargandoModal').fadeOut(300, function () {
				$('#cargandoModal').modal('hide');
			});

			if (datos == "¡Registro editado con exito!") {
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				mostrar();
				$('#nombreEmpresaHeader').text($('#nombreEmpresa').val());
				$('#logoEmpresa').val(null);
			} else {
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}
	});
}






init();