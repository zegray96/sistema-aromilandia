var tabla;
function init() {
	$('#tituloPagina').text('Sistema Aromilandia - Metodos de Pago');
	listar();
	mostrar_form(false);
	$("#wrapper").show();
	$("#preload").hide();

}

// ajustamos las columnas de tabla
$('#sidebarToggleTop').click(function (e) {
	setTimeout(function () {
		tabla.columns.adjust();
	}, 300);
});

//evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach(node => node.addEventListener('keypress', e => {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	}))

	document.querySelectorAll('input[type=number]').forEach(node => node.addEventListener('keypress', e => {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	}))

	document.querySelectorAll('input[type=password]').forEach(node => node.addEventListener('keypress', e => {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	}))
});
// fin enter verificacion

$('#btnNuevo').click(function () {
	mostrar_form(true);
	$('#nombre').focus();
});

$('#btnCancelar').click(function () {
	mostrar_form(false);
});

$("#form").on("submit", function (e) {
	nuevo_editar(e);
});

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
	return str.replace(/\w\S*/g, function (txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

$('#nombre').blur(function () {
	$('#nombre').val(capitalizar($('#nombre').val()));
});

function listar() {
	tabla = $('#tblListado').dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
		"language": {
			"url": "../assets/json/Spanish.json"
		},
		"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons: [
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',

			},


			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',

			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',

			},



		],

		"ajax": {
			url: '../ajax/metodo-pago.php?op=listar',
			type: 'get',
			dataType: 'json',
			error: function (e) {
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength": 25, //cada cuantos registros realizamos la paginacion
		"order": [[1, "asc"]] //para ordenar los registros
	}).DataTable();
}


function mostrar_form(mostrar) {
	limpiar();
	if (mostrar) {
		$('#listado').hide();
		$('#formulario').show();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
		$('#btnNuevo').css("cssText", "display:none !important");
		$('#btnGuardar').prop("disabled", false);
	} else {
		$('#listado').show();
		$('#formulario').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");
		$('#btnNuevo').css("cssText", "display:block !important");

	}
}

function limpiar() {
	$('#idCategoria').val("");
	$('#nombre').val("");
}

function nuevo_editar(e) {
	$('#cargandoModal').modal('show');

	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url: '../ajax/metodo-pago.php?op=nuevo_editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType: false,
		processData: false,

		success: function (datos) {
			$('#cargandoModal').fadeOut(300, function () {
				$('#cargandoModal').modal('hide');
			});

			if (datos == "¡Registro creado con exito!" || datos == "¡Registro editado con exito!") {
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				}).then(function () {
					$('#nombre').focus();
				});
				limpiar();
				tabla.ajax.reload();
			} else {
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}
	});
}

function mostrar(id) {
	$('#cargandoModal').modal('show');
	$.post("../ajax/metodo-pago.php?op=mostrar", { idMetodoPago: id }, function (data, status) {
		try {
			/* Si el JSON está mal formado se generará una excepción */
			data = JSON.parse(data);
			if (data.error == true) {
				/* Si hemos enviado por JSON un error, lo notificamos */
				console.log('ERROR detectado:', data);
				return;
			}
			/* Trabajamos habitualmente con la respuesta */
			$('#cargandoModal').fadeOut(300, function () {
				$('#cargandoModal').modal('hide');
			});
			mostrar_form(true);
			$('#idMetodoPago').val(data.id_metodo_pago);
			$('#nombre').val(data.nombre);


		} catch (error) {
			/* Si el JSON está mal, notificamos su contenido */
			console.log('ERROR. Recibido:', data);
		}
	})
}

function eliminar(id) {
	Swal.fire({
		title: '¿Esta seguro que desea eliminar el registro?',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		showConfirmButton: true,
		confirmButtonText: 'Confirmar',
		cancelButtonText: 'Cancelar',
		reverseButtons: true,
		allowOutsideClick: false
	}).then((result) => {
		if (result.value) {
			$("#cargandoModal").modal('show');
			$.post("../ajax/metodo-pago.php?op=eliminar", { idMetodoPago: id }, function (datos) {
				if (datos == "¡Registro eliminado con exito!") {
					Swal.fire({
						icon: 'success',
						title: datos,
						allowOutsideClick: false
					});
				} else {
					Swal.fire({
						icon: 'error',
						title: datos,
						allowOutsideClick: false
					});
				}

				tabla.ajax.reload();
				$("#cargandoModal").modal('hide');

			});
		}
	});

}



init();