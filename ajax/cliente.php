<?php
require('../models/Cliente.php');

$c = new Cliente();

$codCliente = isset($_POST['codCliente']) ? limpiarCadena($_POST['codCliente']) : "";
$cuit = isset($_POST['cuit']) ? limpiarCadena($_POST['cuit']) : "";
$apellidoNombre = isset($_POST['apellidoNombre']) ? limpiarCadena($_POST['apellidoNombre']) : "";
$dni = isset($_POST['dni']) ? limpiarCadena($_POST['dni']) : "";
$domicilio = isset($_POST['domicilio']) ? limpiarCadena($_POST['domicilio']) : "";
$telefono = isset($_POST['telefono']) ? limpiarCadena($_POST['telefono']) : "";
$localidad = isset($_POST['localidad']) ? limpiarCadena($_POST['localidad']) : "";
$provincia = isset($_POST['provincia']) ? limpiarCadena($_POST['provincia']) : "";
$estado = isset($_POST['estado']) ? limpiarCadena($_POST['estado']) : "";

switch ($_GET['op']) {
    case 'nuevo_editar':
        if ($cuit=="" || $apellidoNombre=="" || $dni=="" || $domicilio=="" || $telefono=="" || $localidad=="" || $provincia=="" || $estado=="") {
            echo "¡Complete los campos obligatorios!";
        } else {
            if (empty($codCliente)) {
                session_start();
                if ($_SESSION['new_clientes']==0) {
                    echo "¡Acción denegada!";
                } else {
                    // Nuevo
                    $respuesta = $c->nuevo($cuit, $apellidoNombre, $dni, $domicilio, $telefono, $localidad, $provincia, $estado);
                    echo $respuesta ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
                }
            } else {
                session_start();
                if ($_SESSION['alt_clientes']==0) {
                    echo "¡Acción denegada!";
                } else {
                    // Editar
                    $respuesta = $c->editar($codCliente, $cuit, $apellidoNombre, $dni, $domicilio, $telefono, $localidad, $provincia, $estado);
                    echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
                }
            }
        }
    break;

    case 'nuevo_en_vista_presupuesto':
        if ($cuit=="" || $apellidoNombre=="" || $dni=="" || $domicilio=="" || $telefono=="" || $localidad=="" || $provincia=="" || $estado=="") {
            echo "¡Complete los campos obligatorios!";
        } else {
            session_start();
            if ($_SESSION['new_clientes']==0) {
                echo "¡Acción denegada!";
            } else {
                // Nuevo
                $respuestaCod = $c->nuevo_en_presupuesto($cuit, $apellidoNombre, $dni, $domicilio, $telefono, $localidad, $provincia, $estado);

                if ($respuestaCod>0) {
                    echo "¡Registro creado con exito!:".$respuestaCod;
                } else {
                    echo "¡Ocurrió un problema y no se pudo crear!:".$respuestaCod;
                }
            }
        }
    break;

    case 'mostrar':
        $respuesta=$c->buscar_cod($codCliente);
        echo json_encode($respuesta);
    break;

    case 'buscar_cliente_manual':
        $respuesta=$c->buscar_cod_manual($codCliente);
        echo json_encode($respuesta);
    break;

    case 'listar':
        session_start();
        $alteracion=$_SESSION['alt_clientes'];
        $respuesta=$c->listar();
        $data = array();

        while ($reg=$respuesta->fetch_object()) {
            if ($alteracion==1) {
                $opciones='<button class="btn btn-warning btn-sm" onclick="mostrar('.$reg->cod_cliente.')"><i class="fas fa-pencil-alt"></i></button>';
            } else {
                $opciones='';
            }

            if ($reg->estado=="ACTIVO") {
                $estado='<span class="badge badge-success">'.$reg->estado.'</span>';
            } else {
                if ($reg->estado=="INACTIVO") {
                    $estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
                }
            }

            $data[]=array(
                "0"=>$opciones,
                "1"=>$reg->cod_cliente,
                "2"=>$estado,
                "3"=>$reg->apellido_nombre,
                "4"=>$reg->cuit,
                "5"=>$reg->dni,
                "6"=>$reg->domicilio,
                "7"=>$reg->telefono,
                "8"=>$reg->localidad,
                "9"=>$reg->provincia,
            );
        }

        $results=array(
            "sEcho"=>1, //informacion para el data table
            "iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
            "aaData"=>$data
        );
        echo json_encode($results);
    break;

}
