<?php
require '../models/Presupuesto.php';
require '../models/Cliente.php';
require '../models/Articulo.php';
require '../models/MetodoPago.php';

$p = new Presupuesto();
$c = new Cliente();
$a = new Articulo();
$m = new MetodoPago();

$idPresupuesto = isset($_POST['idPresupuesto']) ? limpiarCadena($_POST['idPresupuesto']) : "";
$nroPresupuesto = isset($_POST['nroPresupuesto']) ? limpiarCadena($_POST['nroPresupuesto']) : "";
$fechaPresupuesto = isset($_POST['fechaPresupuesto']) ? limpiarCadena($_POST['fechaPresupuesto']) : "";
$codCliente = isset($_POST['codCliente']) ? limpiarCadena($_POST['codCliente']) : "";
$tipoDesc = isset($_POST['tipoDesc']) ? limpiarCadena($_POST['tipoDesc']) : "";
$valorDesc = isset($_POST['valorDesc']) ? limpiarCadena($_POST['valorDesc']) : "";
$montoTotal = isset($_POST['montoTotal']) ? limpiarCadena($_POST['montoTotal']) : "";
$estadoPresu = isset($_POST['estadoPresu']) ? limpiarCadena($_POST['estadoPresu']) : "";
// Arrays
$idMetodoPago = isset($_POST['idMetodoPago']) ? $_POST['idMetodoPago'] : "";
$montoMetodoPago = isset($_POST['montoMetodoPago']) ? $_POST['montoMetodoPago'] : "";
$codArticulo = isset($_POST['codArticulo']) ? $_POST['codArticulo'] : "";
$descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : "";
$precioUnitario = isset($_POST['precioUnitario']) ? $_POST['precioUnitario'] : "";
$cantidad = isset($_POST['cantidad']) ? $_POST['cantidad'] : "";
$subtotal = isset($_POST['subtotal']) ? $_POST['subtotal'] : "";

// para que me pongo formato moneda con , y .
setlocale(LC_MONETARY, 'it_IT');

switch ($_GET['op']) {

    case 'nuevo_editar':
       
        // // formateamos la fecha para guardar en bd
        $parts = explode('/', $fechaPresupuesto);
        $fechaFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
        // formateamos los montos para guardar en bd
        $montoTotalSinComa = str_replace(array('.'), '', $montoTotal);
        $montoTotalSinComa = str_replace(array(','), '.', $montoTotalSinComa);

        // Sumamos los metodos de pago
        $sumaMetodosPago=0.0;
        foreach ($montoMetodoPago as $montoMetPag) {
            $sumaMetodosPago +=$montoMetPag;
        }
      
        // Descuentos
        $descMonto = "";
        $descPorcentaje = "";

        if ($tipoDesc == "monto") {
            $descMonto = $valorDesc;
        } elseif ($tipoDesc == "porcentaje") {
            $descPorcentaje = $valorDesc;
        }

        // Comprobaciones
        if ($fechaFormateada == "" || $codCliente == "" || $estadoPresu == "") {
            echo "¡Complete campos obligatorios!";
        } elseif ($montoTotalSinComa == "" || $montoTotalSinComa < 1) {
            echo "¡Verifique Monto Total!";
        } elseif (($montoMetodoPago=="" || $montoMetodoPago < 1) && $estadoPresu=="PAGADO") {
            echo "¡Ingrese un metodo de pago!";
        } elseif ($sumaMetodosPago!=$montoTotalSinComa && $estadoPresu=="PAGADO") {
            echo "¡La suma de metodos de pago no coincide con el total!";
        } else {
            session_start();
            $idSucursal = $_SESSION['idSucursal'];

            if (empty($idPresupuesto)) {
                // Nuevo
                // si no tengo permisos muestro msj
                if ($_SESSION['new_presupuestos'] == 0) {
                    echo "¡Acción denegada!";
                } else {
                    $respuesta = $p->nuevo($fechaFormateada, $codCliente, $descMonto, $descPorcentaje, $montoTotalSinComa, $idSucursal, $codArticulo, $descripcion, $precioUnitario, $cantidad, $subtotal, $estadoPresu, $idMetodoPago, $montoMetodoPago);
                    // RETORNO
                    $r = explode(":", $respuesta);
                    $resultadoOperacion = $r[0];
                    $resultadoId = $r[1];
                    if ($resultadoOperacion == 1) {
                        echo "¡Registro creado con exito!:" . $resultadoId;
                    } else {
                        echo "¡Ocurrió un problema y no se pudo crear";
                    }
                }
            } else {
                // Editar
                $resp = $p->buscar_id($idPresupuesto);
                $idSucursalBd = $resp['id_sucursal'];

                // si no tengo permisos muestro msj o estoy ingresando con otra sucursal diferente a la que cree el presupuesto
                if ($_SESSION['alt_presupuestos'] == 0 || $idSucursalBd != $idSucursal) {
                    echo "¡Acción denegada!";
                } else {
                    // editar
                    $respuesta = $p->editar($idPresupuesto, $fechaFormateada, $codCliente, $descMonto, $descPorcentaje, $montoTotalSinComa, $idSucursalBd, $codArticulo, $descripcion, $precioUnitario, $cantidad, $subtotal, $estadoPresu, $idMetodoPago, $montoMetodoPago);
                    // RETORNO
                    $r = explode(":", $respuesta);
                    $resultadoOperacion = $r[0];
                    $resultadoId = $r[1];
                    if ($resultadoOperacion == 1) {
                        echo "¡Registro editado con exito!:" . $resultadoId;
                    } else {
                        echo "¡Ocurrió un problema y no se pudo editar";
                    }
                }
            }
        }
           
        break;

    case 'mostrar':
        $respuesta = $p->buscar_id($idPresupuesto);
        echo json_encode($respuesta);
        break;

    case 'eliminar':
        session_start();
        $idSucursal = $_SESSION['idSucursal'];

        if ($_SESSION['del_presupuestos'] == 0) {
            echo "¡Acción denegada!";
        } else {
            $respuesta = $p->eliminar($idPresupuesto, $idSucursal);
            echo $respuesta ? "¡Registro eliminado con exito!" : "¡Ocurrió un problema y no se pudo eliminar!";
        }

        break;

    case 'listar':
        session_start();
        $alteracion = $_SESSION['alt_presupuestos'];
        $eliminacion = $_SESSION['del_presupuestos'];
        $idSucursal = $_SESSION['idSucursal'];
        $respuesta = $p->listar();
        $data = array();

        while ($reg = $respuesta->fetch_object()) {
            if ($alteracion == 1 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
            } else {
                if ($alteracion == 1 && $eliminacion == 0 && $idSucursal == $reg->id_sucursal) {
                    $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                } else {
                    if ($alteracion == 0 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    } else {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    }
                }
            }

            if ($reg->estado == "PAGADO") {
                $estado = '<span class="badge badge-success">' . $reg->estado . '</span>';
            } else {
                $estado = '<span class="badge badge-danger">' . $reg->estado . '</span>';
            }

            if ($reg->monto_total <= 9999.99) {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '');
            } else {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '.');
            }

            $resultado = $p->listar_metodos_pago($reg->id_presupuesto);
            $metodosPagoYdesc="";
            while ($r = $resultado->fetch_object()) {
                if ($r->monto <= 9999.99) {
                    $monto = "$" . number_format($r->monto, 2, ',', '');
                } else {
                    $monto = "$" . number_format($r->monto, 2, ',', '.');
                }

                $metodosPagoYdesc.='&#8226; ' .$r->metodo_pago_nombre.' <strong>'.$monto.'</strong><br>';
            }
            
            if ($reg->desc_porcentaje!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: '.$reg->desc_porcentaje.'%</span><h5>';
            }

            if ($reg->desc_monto!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: $'.$reg->desc_monto.'</span><h5>';
            }

            $data[] = array(
                "0" => $opciones,
                "1" => $estado,
                "2" => $reg->fecha_presupuesto,
                "3" => date("H:i", strtotime($reg->hora_presupuesto)),
                "4" => str_pad($reg->nro_presupuesto, 8, "0", STR_PAD_LEFT),
                "5" => $reg->apellidoNombre,
                "6" => $reg->dni,
                "7" => $montoTotal,
                "8" => $metodosPagoYdesc,
                "9" => $reg->sucursal,
            );



            $data[] = array(
                "0" => $opciones,
                "1" => $estado,
                "2" => $reg->fecha_presupuesto,
                "3" => date("H:i", strtotime($reg->hora_presupuesto)),
                "4" => str_pad($reg->nro_presupuesto, 8, "0", STR_PAD_LEFT),
                "5" => $reg->apellidoNombre,
                "6" => $reg->dni,
                "7" => $montoTotal,
                "8" => $metodosPago,
                "9" => $reg->sucursal,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

    case 'listar_hoy':
        session_start();
        $alteracion = $_SESSION['alt_presupuestos'];
        $eliminacion = $_SESSION['del_presupuestos'];
        $idSucursal = $_SESSION['idSucursal'];
        $respuesta = $p->listar_hoy();
        $data = array();

        while ($reg = $respuesta->fetch_object()) {
            if ($alteracion == 1 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
            } else {
                if ($alteracion == 1 && $eliminacion == 0 && $idSucursal == $reg->id_sucursal) {
                    $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                } else {
                    if ($alteracion == 0 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    } else {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    }
                }
            }

            if ($reg->estado == "PAGADO") {
                $estado = '<span class="badge badge-success">' . $reg->estado . '</span>';
            } else {
                $estado = '<span class="badge badge-danger">' . $reg->estado . '</span>';
            }

            if ($reg->monto_total <= 9999.99) {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '');
            } else {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '.');
            }

            $resultado = $p->listar_metodos_pago($reg->id_presupuesto);
            $metodosPagoYdesc="";
            while ($r = $resultado->fetch_object()) {
                if ($r->monto <= 9999.99) {
                    $monto = "$" . number_format($r->monto, 2, ',', '');
                } else {
                    $monto = "$" . number_format($r->monto, 2, ',', '.');
                }

                $metodosPagoYdesc.='&#8226; ' .$r->metodo_pago_nombre.' <strong>'.$monto.'</strong><br>';
            }
            
            if ($reg->desc_porcentaje!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: '.$reg->desc_porcentaje.'%</span><h5>';
            }

            if ($reg->desc_monto!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: $'.$reg->desc_monto.'</span><h5>';
            }

            $data[] = array(
                "0" => $opciones,
                "1" => $estado,
                "2" => $reg->fecha_presupuesto,
                "3" => date("H:i", strtotime($reg->hora_presupuesto)),
                "4" => str_pad($reg->nro_presupuesto, 8, "0", STR_PAD_LEFT),
                "5" => $reg->apellidoNombre,
                "6" => $reg->dni,
                "7" => $montoTotal,
                "8" => $metodosPagoYdesc,
                "9" => $reg->sucursal,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

    case 'filtrar_por_fecha':
        $fechaIni = $_REQUEST['fechaIni'];
        $fechaFin = $_REQUEST['fechaFin'];

        $parts = explode('/', $fechaIni);
        $fechaIniFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

        $parts = explode('/', $fechaFin);
        $fechaFinFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

        session_start();
        $alteracion = $_SESSION['alt_presupuestos'];
        $eliminacion = $_SESSION['del_presupuestos'];
        $idSucursal = $_SESSION['idSucursal'];
        $respuesta = $p->listar_por_fecha($fechaIniFormateada, $fechaFinFormateada);
        $data = array();

        while ($reg = $respuesta->fetch_object()) {
            if ($alteracion == 1 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
            } else {
                if ($alteracion == 1 && $eliminacion == 0 && $idSucursal == $reg->id_sucursal) {
                    $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                } else {
                    if ($alteracion == 0 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    } else {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    }
                }
            }

            if ($reg->estado == "PAGADO") {
                $estado = '<span class="badge badge-success">' . $reg->estado . '</span>';
            } else {
                $estado = '<span class="badge badge-danger">' . $reg->estado . '</span>';
            }

            if ($reg->monto_total <= 9999.99) {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '');
            } else {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '.');
            }

            $resultado = $p->listar_metodos_pago($reg->id_presupuesto);
            $metodosPagoYdesc="";
            while ($r = $resultado->fetch_object()) {
                if ($r->monto <= 9999.99) {
                    $monto = "$" . number_format($r->monto, 2, ',', '');
                } else {
                    $monto = "$" . number_format($r->monto, 2, ',', '.');
                }

                $metodosPagoYdesc.='&#8226; ' .$r->metodo_pago_nombre.' <strong>'.$monto.'</strong><br>';
            }
            
            if ($reg->desc_porcentaje!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: '.$reg->desc_porcentaje.'%</span><h5>';
            }

            if ($reg->desc_monto!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: $'.$reg->desc_monto.'</span><h5>';
            }

            $data[] = array(
                "0" => $opciones,
                "1" => $estado,
                "2" => $reg->fecha_presupuesto,
                "3" => date("H:i", strtotime($reg->hora_presupuesto)),
                "4" => str_pad($reg->nro_presupuesto, 8, "0", STR_PAD_LEFT),
                "5" => $reg->apellidoNombre,
                "6" => $reg->dni,
                "7" => $montoTotal,
                "8" => $metodosPagoYdesc,
                "9" => $reg->sucursal,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

    case 'listar_clientes_para_presu':

        $respuesta = $c->listar_activos();
        //declaramos un array
        $data = array();
        while ($reg = $respuesta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-success btn-sm" onclick="agregar_cliente(' . $reg->cod_cliente . ',\'' . $reg->dni . '\',\'' . $reg->apellido_nombre . '\',\'' . $reg->cuit . '\')"><i class="fas fa-arrow-circle-right"></i></button>',
                "1" => $reg->apellido_nombre,
                "2" => $reg->cuit,
                "3" => $reg->dni,
                "4" => $reg->domicilio,
                "5" => $reg->localidad,
                "6" => $reg->provincia,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

    case 'filtrar_por_cliente':

        $respuesta = $c->listar_activos();
        //declaramos un array
        $data = array();
        while ($reg = $respuesta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-success btn-sm" onclick="listar_presupuestos_por_cliente_filtro(' . $reg->cod_cliente . ')"><i class="fas fa-arrow-circle-right"></i></button>',
                "1" => $reg->apellido_nombre,
                "2" => $reg->cuit,
                "3" => $reg->dni,
                "4" => $reg->domicilio,
                "5" => $reg->localidad,
                "6" => $reg->provincia,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

    case 'listar_detalles':
        $id = $_GET['id'];
        $resultado = $p->listar_detalles($id);
        
        echo '<thead class="thead-dark">
				<th><i class="fas fa-th-list ml-2 mr-2"></i></th>
				<th>Descripción</th>
				<th>Cantidad</th>
				<th>Precio&nbsp;Unit.</th>
				<th>Subtotal</th>
			</thead>';
        $idFila = 0;
        while ($reg = $resultado->fetch_object()) {

            // Traemos la imagen
            $respuestaArticulo = $a->buscar_img_articulo($reg->cod_articulo);
            $imagen = $respuestaArticulo['imagen'];
            if ($imagen == "") {
                $imagen = "sin-imagen.jpg";
            }

            if ($reg->precio_unitario <= 9999.99) {
                $precioUnitario = "$" . number_format($reg->precio_unitario, 2, ',', '');
            } else {
                $precioUnitario = "$" . number_format($reg->precio_unitario, 2, ',', '.');
            }

            if ($reg->subtotal <= 9999.99) {
                $subtotal = "$" . number_format($reg->subtotal, 2, ',', '');
            } else {
                $subtotal = "$" . number_format($reg->subtotal, 2, ',', '.');
            }

            echo '<tr class="filas" id="fila' . $idFila . '">' .
            '<td style="display: none"><input type="hidden" name="codArticulo[]" value="' . $reg->cod_articulo . '">' . $reg->cod_articulo . '</td>' .
            '<td style="display: none"><input type="hidden" name="imgArticulo[]" value="' . $imagen . "?" . rand() . '">' . $imagen . "?" . rand() . '</td>' .
            '<td><button type="button" class="btn btn-danger btn-sm" onclick="borrar_detalle(' . $idFila . ')"><i class="fas fa-trash-alt"></i></button>' .
            ' <button type="button" class="btnVerDetalle btn btn-info btn-sm"><i class="fas fa-eye"></i></button></td>' .

            '<td class="nr"><input type="hidden" name="descripcion[]" value="' . $reg->descripcion . '">' . $reg->descripcion . '</td>' .
            '<td><input type="hidden" name="cantidad[]" value="' . $reg->cantidad . '">' . $reg->cantidad . '</td>' .
            '<td><input class="precioUnitario" type="hidden" name="precioUnitario[]" value="' . $reg->precio_unitario . '">' . $precioUnitario . '</td>' .
            '<td><input type="hidden" name="subtotal[]" value="' . $reg->subtotal . '">' . $subtotal . '</td>' .
                '</tr>';
            $idFila++;
        }
        $idFila--;
        echo '<input type="hidden" id="idFila" value="' . $idFila . '">';

        break;

    case 'listar_metodos_pago':
        $id = $_GET['id'];
        $resultado = $p->listar_metodos_pago($id);
        
        echo '<thead class="thead-dark">
                <th id="opMetodoPago"><button onclick="abrirAgregarMetodoPago()" type="button" class="btn btn-success btn-sm"><i
                    class="fas fa-plus-circle"></i></button></th>
                <th>Metodo&nbsp;de&nbsp;Pago</th>
                <th>Monto</th>
            </thead>';
        $idFilaMetodoPago = 0;
        while ($reg = $resultado->fetch_object()) {
            if ($reg->monto <= 9999.99) {
                $monto = "$" . number_format($reg->monto, 2, ',', '');
            } else {
                $monto = "$" . number_format($reg->monto, 2, ',', '.');
            }

            
            
            echo '<tr class="filasMetodosPago" id="filaMetodoPago' . $idFilaMetodoPago . '">' .
            '<td style="display: none"><input type="hidden" name="idMetodoPago[]" value="'.$reg->id_metodo_pago.'">'.$reg->id_metodo_pago.'</td>' .
            '<td><button type="button" class="btn btn-danger btn-sm" onclick="borrar_metodo_pago('.$idFilaMetodoPago.')"><i class="fas fa-trash-alt"></i></button></td>' .
            '<td><input type="hidden" name="metodoPagoNombre[]" value="'.$reg->metodo_pago_nombre.'">'.$reg->metodo_pago_nombre.'</td>' .
            '<td><input type="hidden" name="montoMetodoPago[]" value="'.$reg->monto.'">'.$monto.'</td></tr>';
            $idFilaMetodoPago++;
        }

        if (mysqli_num_rows($resultado)>0) {
            $idFilaMetodoPago--;
            echo '<input type="hidden" id="idFilaMetodoPago" value="' . $idFilaMetodoPago . '">';
        } else {
            echo '<tbody>
            <input type="hidden" id="idFilaMetodoPago" value="' . $idFilaMetodoPago . '">
            </tbody>';
        }
        

        break;
    

    case 'listar_articulos':
        session_start();
        $idSucursal = $_SESSION['idSucursal'];
        $respuesta = $a->listar_activos($idSucursal);
        //declaramos un array
        $data = array();
        while ($reg = $respuesta->fetch_object()) {
            if ($reg->stock_actual == null) {
                $stock_actual = 0;
            } else {
                $stock_actual = $reg->stock_actual;
            }

            if (!$reg->imagen == "") {
                $imagen = "<a href='../../imgArticulos/" . $reg->imagen . "?" . rand() . "' target='_blank'><img class='thumbnail-imagen' src='../imgArticulos/" . $reg->imagen . "?" . rand() . "' width='90px' height='90px'></i></a>";
            } else {
                $imagen = "<img class='thumbnail-imagen' src='../imgArticulos/sin-imagen.jpg?" . rand() . "' width='90px' height='90px'></i>";
            }

            $data[] = array(
                "0" => '<div class="input-group-prepend">
                      <button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"></button>
                      <div class="dropdown-menu">
                        <h5><a class="dropdown-item badge badge-danger text-white" style="cursor: pointer;" onclick="agregar_articulo(' . $reg->cod_articulo . ',\'' . $reg->descripcion . '\',\'' . number_format($reg->precio_unitario_mayorista, 2, ',', '.') . '\',\'M\',\'' . $reg->imagen . '\')">Mayorista $' . number_format($reg->precio_unitario_mayorista, 2, ',', '.') . '</a></h5>
                        <h5><a class="dropdown-item badge badge-success text-white" style="cursor: pointer;" onclick="agregar_articulo(' . $reg->cod_articulo . ',\'' . $reg->descripcion . '\',\'' . number_format($reg->precio_unitario_publico, 2, ',', '.') . '\',\'P\',\'' . $reg->imagen . '\')">Público $' . number_format($reg->precio_unitario_publico, 2, ',', '.') . '</a></h5>
                      </div>
                    </div>',
                "1" => $reg->cod_articulo,
                "2" => $reg->descripcion,
                "3" => $imagen,
                "4" => $stock_actual,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

    case 'listar_presupuestos_por_cliente_presup':
        session_start();
        $alteracion = $_SESSION['alt_presupuestos'];
        $idSucursal = $_SESSION['idSucursal'];
        $codCliente = $_REQUEST['codCliente'];
        $respuesta = $p->listar_presupuestos_por_cliente($codCliente);
        $data = array();

        while ($reg = $respuesta->fetch_object()) {
            if ($alteracion == 1 && $idSucursal == $reg->id_sucursal) {
                $opciones = '<button class="btn btn-warning btn-sm" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i></button>';
            } else {
                $opciones = '';
            }

            if ($reg->estado == "PAGADO") {
                $estado = '<span class="badge badge-success">' . $reg->estado . '</span>';
            } else {
                $estado = '<span class="badge badge-danger">' . $reg->estado . '</span>';
            }

            if ($reg->monto_total <= 9999.99) {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '');
            } else {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '.');
            }

            $data[] = array(
                "0" => $opciones,
                "1" => $estado,
                "2" => $reg->fecha_presupuesto,
                "3" => str_pad($reg->nro_presupuesto, 8, "0", STR_PAD_LEFT),
                "4" => $montoTotal,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

    case 'listar_presupuestos_por_cliente_filtro':
        session_start();
        $alteracion = $_SESSION['alt_presupuestos'];
        $eliminacion = $_SESSION['del_presupuestos'];
        $idSucursal = $_SESSION['idSucursal'];
        $codCliente = $_REQUEST['codCliente'];
        $respuesta = $p->listar_presupuestos_por_cliente($codCliente);
        $data = array();

        while ($reg = $respuesta->fetch_object()) {
            if ($alteracion == 1 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
            } else {
                if ($alteracion == 1 && $eliminacion == 0 && $idSucursal == $reg->id_sucursal) {
                    $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                } else {
                    if ($alteracion == 0 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    } else {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    }
                }
            }

            if ($reg->estado == "PAGADO") {
                $estado = '<span class="badge badge-success">' . $reg->estado . '</span>';
            } else {
                $estado = '<span class="badge badge-danger">' . $reg->estado . '</span>';
            }

            if ($reg->monto_total <= 9999.99) {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '');
            } else {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '.');
            }

            $resultado = $p->listar_metodos_pago($reg->id_presupuesto);
            $metodosPagoYdesc="";
            while ($r = $resultado->fetch_object()) {
                if ($r->monto <= 9999.99) {
                    $monto = "$" . number_format($r->monto, 2, ',', '');
                } else {
                    $monto = "$" . number_format($r->monto, 2, ',', '.');
                }

                $metodosPagoYdesc.='&#8226; ' .$r->metodo_pago_nombre.' <strong>'.$monto.'</strong><br>';
            }
            
            if ($reg->desc_porcentaje!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: '.$reg->desc_porcentaje.'%</span><h5>';
            }

            if ($reg->desc_monto!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: $'.$reg->desc_monto.'</span><h5>';
            }

            $data[] = array(
                "0" => $opciones,
                "1" => $estado,
                "2" => $reg->fecha_presupuesto,
                "3" => date("H:i", strtotime($reg->hora_presupuesto)),
                "4" => str_pad($reg->nro_presupuesto, 8, "0", STR_PAD_LEFT),
                "5" => $reg->apellidoNombre,
                "6" => $reg->dni,
                "7" => $montoTotal,
                "8" => $metodosPagoYdesc,
                "9" => $reg->sucursal,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

    case 'cargar_metodos_pago':
        $metodosPago=$m->listar();
        while ($reg=$metodosPago->fetch_object()) {
            echo '<option value='.$reg->id_metodo_pago.'>'.$reg->nombre.'</option>';
        }
        break;

    case 'cargar_sucursales':
        $resultado=$p->listar_sucursales();
        while ($reg=$resultado->fetch_object()) {
            echo '<option value='.$reg->id_sucursal.'>'.$reg->nombre.'</option>';
        }
    break;

    case 'filtrar_por_sucursal':
        session_start();
        $alteracion = $_SESSION['alt_presupuestos'];
        $eliminacion = $_SESSION['del_presupuestos'];
        $idSucursal = $_SESSION['idSucursal'];
        $idSucursalRequest=$_REQUEST['idSucursal'];
        $fechaIni=$_REQUEST['fechaIni'];
        $fechaFin=$_REQUEST['fechaFin'];
        

        $parts = explode('/', $fechaIni);
        $fechaIniFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

        $parts = explode('/', $fechaFin);
        $fechaFinFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

        $respuesta=$p->listar_por_sucursal($idSucursalRequest, $fechaIniFormateada, $fechaFinFormateada);


        $data = array();
        while ($reg = $respuesta->fetch_object()) {
            if ($alteracion == 1 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
            } else {
                if ($alteracion == 1 && $eliminacion == 0 && $idSucursal == $reg->id_sucursal) {
                    $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="mostrar(' . $reg->id_presupuesto . ')"><i class="fas fa-pencil-alt"></i> Editar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                } else {
                    if ($alteracion == 0 && $eliminacion == 1 && $idSucursal == $reg->id_sucursal) {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="eliminar_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-trash-alt"></i> Eliminar</a></h6>
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    } else {
                        $opciones = '<div class="dropdown">
							  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
							    Opciones
							  </button>
							  <div class="dropdown-menu">
							    <h6><a class="dropdown-item" style="cursor:pointer; color:#3a3b45;" onclick="imprimir_presupuesto(' . $reg->id_presupuesto . ')"><i class="fas fa-print"></i> Imprimir</a></h6>
							  </div>
							</div>';
                    }
                }
            }

            if ($reg->estado == "PAGADO") {
                $estado = '<span class="badge badge-success">' . $reg->estado . '</span>';
            } else {
                $estado = '<span class="badge badge-danger">' . $reg->estado . '</span>';
            }

            if ($reg->monto_total <= 9999.99) {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '');
            } else {
                $montoTotal = "$" . number_format($reg->monto_total, 2, ',', '.');
            }

            $resultado = $p->listar_metodos_pago($reg->id_presupuesto);
            $metodosPagoYdesc="";
            while ($r = $resultado->fetch_object()) {
                if ($r->monto <= 9999.99) {
                    $monto = "$" . number_format($r->monto, 2, ',', '');
                } else {
                    $monto = "$" . number_format($r->monto, 2, ',', '.');
                }

                $metodosPagoYdesc.='&#8226; ' .$r->metodo_pago_nombre.' <strong>'.$monto.'</strong><br>';
            }
            
            if ($reg->desc_porcentaje!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: '.$reg->desc_porcentaje.'%</span><h5>';
            }

            if ($reg->desc_monto!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: $'.$reg->desc_monto.'</span><h5>';
            }

            $data[] = array(
                "0" => $opciones,
                "1" => $estado,
                "2" => $reg->fecha_presupuesto,
                "3" => date("H:i", strtotime($reg->hora_presupuesto)),
                "4" => str_pad($reg->nro_presupuesto, 8, "0", STR_PAD_LEFT),
                "5" => $reg->apellidoNombre,
                "6" => $reg->dni,
                "7" => $montoTotal,
                "8" => $metodosPagoYdesc,
                "9" => $reg->sucursal,
            );
        }

        $results=array(
            "sEcho"=>1, //informacion para el data table
            "iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
            "aaData"=>$data
        );
        echo json_encode($results);



    break;

}