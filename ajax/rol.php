<?php
require('../models/Rol.php');

$r = new Rol();

$idRol = isset($_POST['idRol']) ? limpiarCadena($_POST['idRol']) : "";
$nombre = isset($_POST['nombre']) ? limpiarCadena($_POST['nombre']) : "";
$vConfiguracion=isset($_POST['vConfiguracion'])? 1:0;
$vAcceso=isset($_POST['vAcceso'])? 1:0;
$vClientes=isset($_POST['vClientes'])? 1:0;
$vArticulos=isset($_POST['vArticulos'])? 1:0;
$vCategoriasArt=isset($_POST['vCategoriasArt'])? 1:0;
$vPresupuestos=isset($_POST['vPresupuestos'])? 1:0;
$vMetodosPago=isset($_POST['vMetodosPago'])? 1:0;
$newAcceso=isset($_POST['newAcceso'])? 1:0;
$newClientes=isset($_POST['newClientes'])? 1:0;
$newArticulos=isset($_POST['newArticulos'])? 1:0;
$newCategoriasArt=isset($_POST['newArticulos'])? 1:0;
$newPresupuestos=isset($_POST['newPresupuestos'])? 1:0;
$newMetodosPago=isset($_POST['newMetodosPago'])? 1:0;
$altAcceso=isset($_POST['altAcceso'])? 1:0;
$altClientes=isset($_POST['altClientes'])? 1:0;
$altArticulos=isset($_POST['altArticulos'])? 1:0;
$altCategoriasArt=isset($_POST['altCategoriasArt'])? 1:0;
$altPresupuestos=isset($_POST['altPresupuestos'])? 1:0;
$altMetodosPago=isset($_POST['altMetodosPago'])? 1:0;
$delPresupuestos=isset($_POST['delPresupuestos'])? 1:0;
$delCategoriasArt=isset($_POST['delCategoriasArt'])? 1:0;
$delMetodosPago=isset($_POST['delMetodosPago'])? 1:0;

switch ($_GET['op']) {

    case 'nuevo_editar':
        if ($idRol==1) {
            echo "¡No puede editar rol Super Admin!";
        } else {
            if ($nombre=="") {
                echo "¡Complete los campos obligatorios!";
            } else {
                $verificacion=null;
                $nombreBd="";
                
                if (!empty($idRol)) {
                    $respuesta=$r->buscar_id($idRol);
                    $nombreBd=$respuesta['nombre'];
                }

                if ($nombre!=$nombreBd) {
                    $verificacion=$r->verificar_existencia_nombre($nombre);
                }


                if (!empty($verificacion)) {
                    echo "¡Nombre ya existe!";
                } else {
                    if (empty($idRol)) {
                        session_start();
                        if ($_SESSION['new_acceso']==0) {
                            echo "¡Acción denegada!";
                        } else {
                            // Nuevo
                            $respuesta = $r->nuevo(
                                $nombre,
                                $vAcceso,
                                $vClientes,
                                $vArticulos,
                                $vCategoriasArt,
                                $vPresupuestos,
                                $vConfiguracion,
                                $vMetodosPago,
                                $altAcceso,
                                $altClientes,
                                $altArticulos,
                                $altCategoriasArt,
                                $altPresupuestos,
                                $altMetodosPago,
                                $newAcceso,
                                $newClientes,
                                $newArticulos,
                                $newCategoriasArt,
                                $newPresupuestos,
                                $newMetodosPago,
                                $delPresupuestos,
                                $delCategoriasArt,
                                $delMetodosPago
                            );
                            echo $respuesta ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
                        }
                    } else {
                        session_start();
                        if ($_SESSION['alt_acceso']==0) {
                            echo "¡Acción denegada!";
                        } else {
                            // Editar
                            $respuesta = $r->editar(
                                $idRol,
                                $nombre,
                                $vAcceso,
                                $vClientes,
                                $vArticulos,
                                $vCategoriasArt,
                                $vPresupuestos,
                                $vConfiguracion,
                                $vMetodosPago,
                                $altAcceso,
                                $altClientes,
                                $altArticulos,
                                $altCategoriasArt,
                                $altPresupuestos,
                                $altMetodosPago,
                                $newAcceso,
                                $newClientes,
                                $newArticulos,
                                $newCategoriasArt,
                                $newPresupuestos,
                                $newMetodosPago,
                                $delPresupuestos,
                                $delCategoriasArt,
                                $delMetodosPago
                            );
                            echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
                        }
                    }
                }
            }
        }
            
    break;

    case 'mostrar':
        $respuesta=$r->buscar_id($idRol);
        echo json_encode($respuesta);
    break;

    case 'listar':
        session_start();
        $alteracion=$_SESSION['alt_acceso'];
        $respuesta=$r->listar();
        $data = array();

        while ($reg=$respuesta->fetch_object()) {
            if ($alteracion==1 && $reg->id_rol!=1) {
                $opciones='<button class="btn btn-warning btn-sm" onclick="mostrar('.$reg->id_rol.')"><i class="fas fa-pencil-alt"></i></button>';
            } else {
                $opciones='';
            }

            $data[]=array(
                "0"=>$opciones,
                "1"=>$reg->nombre,
            );
        }

        $results=array(
            "sEcho"=>1, //informacion para el data table
            "iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
            "aaData"=>$data
        );
        echo json_encode($results);
    break;

}