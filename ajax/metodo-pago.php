<?php
require('../models/MetodoPago.php');

$m = new MetodoPago();

$idMetodoPago = isset($_POST['idMetodoPago']) ? limpiarCadena($_POST['idMetodoPago']) : "";
$nombre = isset($_POST['nombre']) ? limpiarCadena($_POST['nombre']) : "";

switch ($_GET['op']) {

    case 'nuevo_editar':
        
        if ($nombre=="") {
            echo "¡Complete los campos obligatorios!";
        } else {
            $verificacion=null;
            $nombreBd="";
            
            if (!empty($idMetodoPago)) {
                $respuesta=$m->buscar_id($idMetodoPago);
                $nombreBd=$respuesta['nombre'];
            }

            if ($nombre!=$nombreBd) {
                $verificacion=$m->verificar_existencia_nombre($nombre);
            }


            if (!empty($verificacion)) {
                echo "¡Nombre ya existe!";
            } else {
                if (empty($idMetodoPago)) {
                    session_start();
                    if ($_SESSION['new_metodos_pago']==0) {
                        echo "¡Acción denegada!";
                    } else {
                        // Nuevo
                        $respuesta = $m->nuevo($nombre);
                        echo $respuesta ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
                    }
                } else {
                    session_start();
                    if ($_SESSION['alt_metodos_pago']==0) {
                        echo "¡Acción denegada!";
                    } else {
                        // Editar
                        $respuesta = $m->editar($idMetodoPago, $nombre);
                        echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
                    }
                }
            }
        }
    
            
    break;

    case 'mostrar':
        $respuesta=$m->buscar_id($idMetodoPago);
        echo json_encode($respuesta);
    break;

    case 'eliminar':
        session_start();
        $eliminacion=$_SESSION['del_metodos_pago'];
        if ($eliminacion==1) {
            $verificacion="";
            $verificacion=$m->buscar_en_presupuestos_metodos_pago($idMetodoPago);
            if ($verificacion['cantidad']>0) {
                echo "¡El registro se encuentra utilizado en el sistema!";
            } else {
                $respuesta=$m->eliminar($idMetodoPago);
                echo $respuesta ? "¡Registro eliminado con exito!" : "¡Ocurrió un problema y no se pudo eliminar!";
            }
        } else {
            echo "¡Acción denegada!";
        }
        
    break;

    case 'listar':
        session_start();
        $alteracion=$_SESSION['alt_metodos_pago'];
        $eliminacion=$_SESSION['del_metodos_pago'];
        $respuesta=$m->listar();
        $data = array();

        while ($reg=$respuesta->fetch_object()) {
            $opciones="";
            if ($alteracion==1) {
                $opciones.='<button class="btn btn-warning btn-sm" onclick="mostrar('.$reg->id_metodo_pago.')"><i class="fas fa-pencil-alt"></i></button>';
            }
            if ($eliminacion==1) {
                $opciones.=' <button class="btn btn-danger btn-sm" onclick="eliminar('.$reg->id_metodo_pago.')"><i class="fas fa-trash-alt"></i></button>';
            }

            $data[]=array(
                "0"=>$opciones,
                "1"=>$reg->nombre,
            );
        }

        $results=array(
            "sEcho"=>1, //informacion para el data table
            "iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
            "aaData"=>$data
        );
        echo json_encode($results);
    break;

}