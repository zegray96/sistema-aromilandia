<?php
require('../models/Usuario.php');
$u = new Usuario();

session_start();

$usuario=isset($_POST['usuario']) ? limpiarCadena($_POST['usuario']) : "";
$clave=isset($_POST['clave']) ? limpiarCadena($_POST['clave']) : "";

switch ($_GET['op']) {
    case 'iniciar_sesion':
        $claveHash = hash("SHA256", $clave);
        $respuesta = $u->verificar_login($usuario, $claveHash);
        $reg = $respuesta->fetch_object();
        if (isset($reg)) {
            $_SESSION['idUsuarioSisArom']=$reg->id_usuario;
            $_SESSION['apellidoNombre']=$reg->apellido_nombre;
            $_SESSION['usuario'] = $reg->usuario;
            $_SESSION['clave'] = $reg->clave;
            $_SESSION['idRol'] = $reg->id_rol;
            $_SESSION['idSucursal'] = $reg->id_sucursal;
            $_SESSION['sucursal'] = $reg->sucursal;
            $_SESSION['rol'] = $reg->rol;
            $_SESSION['estado'] = $reg->estado;
            $_SESSION['v_acceso'] = $reg->v_acceso;
            $_SESSION['v_clientes'] = $reg->v_clientes;
            $_SESSION['v_presupuestos'] = $reg->v_presupuestos;
            $_SESSION['v_articulos'] = $reg->v_articulos;
            $_SESSION['v_categorias_art'] = $reg->v_categorias_art;
            $_SESSION['v_configuracion'] = $reg->v_configuracion;
            $_SESSION['v_metodos_pago'] = $reg->v_metodos_pago;
            $_SESSION['alt_acceso'] = $reg->alt_acceso;
            $_SESSION['alt_clientes'] = $reg->alt_clientes;
            $_SESSION['alt_presupuestos'] = $reg->alt_presupuestos;
            $_SESSION['alt_articulos'] = $reg->alt_articulos;
            $_SESSION['alt_categorias_art'] = $reg->alt_categorias_art;
            $_SESSION['alt_metodos_pago'] = $reg->alt_metodos_pago;
            $_SESSION['new_acceso'] = $reg->new_acceso;
            $_SESSION['new_clientes'] = $reg->new_clientes;
            $_SESSION['new_presupuestos'] = $reg->new_presupuestos;
            $_SESSION['new_articulos'] = $reg->new_articulos;
            $_SESSION['new_categorias_art'] = $reg->new_categorias_art;
            $_SESSION['new_metodos_pago'] = $reg->new_metodos_pago;
            $_SESSION['del_presupuestos'] = $reg->del_presupuestos;
            $_SESSION['del_categorias_art'] = $reg->del_categorias_art;
            $_SESSION['del_metodos_pago'] = $reg->del_metodos_pago;
        }
        echo json_encode($reg);
    break;

    case 'cerrar_sesion':
        session_unset();
        session_destroy();
    break;
    
    
}