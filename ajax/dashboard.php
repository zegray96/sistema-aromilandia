<?php
require('../models/Dashboard.php');
require('../models/Presupuesto.php');
$d = new Dashboard();
$p = new Presupuesto();

switch ($_GET['op']) {

    case 'cargar_sucursales':
        $resultado=$d->listar_sucursales();
        while ($reg=$resultado->fetch_object()) {
            echo '<option value='.$reg->id_sucursal.'>'.$reg->nombre.'</option>';
        }
    break;

    case 'calcular_total':
        $idSucursal=$_REQUEST['idSucursal'];
        $fechaIni=$_REQUEST['fechaIni'];
        $fechaFin=$_REQUEST['fechaFin'];

        $parts = explode('/', $fechaIni);
        $fechaIniFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

        $parts = explode('/', $fechaFin);
        $fechaFinFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

        $respuesta=$d->calcular_total($idSucursal, $fechaIniFormateada, $fechaFinFormateada);


        $data = array();
        while ($reg=$respuesta->fetch_object()) {
            if ($reg->estado=="PAGADO") {
                $estado='<span class="badge badge-success">'.$reg->estado.'</span>';
            } else {
                $estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
            }

            if ($reg->monto_total<=9999.99) {
                $montoTotal="$".number_format($reg->monto_total, 2, ',', '');
            } else {
                $montoTotal="$".number_format($reg->monto_total, 2, ',', '.');
            }

            $resultado = $p->listar_metodos_pago($reg->id_presupuesto);
            $metodosPagoYdesc="";
            while ($r = $resultado->fetch_object()) {
                if ($r->monto <= 9999.99) {
                    $monto = "$" . number_format($r->monto, 2, ',', '');
                } else {
                    $monto = "$" . number_format($r->monto, 2, ',', '.');
                }

                $metodosPagoYdesc.='&#8226; ' .$r->metodo_pago_nombre.' <strong>'.$monto.'</strong><br>';
            }
            
            if ($reg->desc_porcentaje!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: '.$reg->desc_porcentaje.'%</span><h5>';
            }

            if ($reg->desc_monto!=0) {
                $metodosPagoYdesc.=' <h5><span class="badge badge-warning"> Descuento: $'.$reg->desc_monto.'</span><h5>';
            }

            $data[]=array(
                "0"=>$estado,
                "1"=>$reg->fecha_presupuesto,
                "2"=>$reg->hora_presupuesto,
                "3"=>str_pad($reg->nro_presupuesto, 8, "0", STR_PAD_LEFT),
                "4"=>$reg->apellidoNombre,
                "5"=>$montoTotal,
                "6" => $metodosPagoYdesc,
            );
        }

        $results=array(
            "sEcho"=>1, //informacion para el data table
            "iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
            "aaData"=>$data
        );
        echo json_encode($results);



    break;

    case 'traer_suma_total':
        $idSucursal=$_GET['idSucursal'];
        $fechaIni=$_GET['fechaIni'];
        $fechaFin=$_GET['fechaFin'];

        $parts = explode('/', $fechaIni);
        $fechaIniFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

        $parts = explode('/', $fechaFin);
        $fechaFinFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

        $respuesta=$d->traer_suma_total($idSucursal, $fechaIniFormateada, $fechaFinFormateada);

        echo $respuesta['suma'];
    break;
}