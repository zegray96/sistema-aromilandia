<?php
require('../models/CategoriaArticulo.php');

$c = new CategoriaArticulo();

$idCategoria = isset($_POST['idCategoria']) ? limpiarCadena($_POST['idCategoria']) : "";
$nombre = isset($_POST['nombre']) ? limpiarCadena($_POST['nombre']) : "";

switch ($_GET['op']) {

    case 'nuevo_editar':
        
        if ($nombre=="") {
            echo "¡Complete los campos obligatorios!";
        } else {
            $verificacion=null;
            $nombreBd="";
            
            if (!empty($idCategoria)) {
                $respuesta=$c->buscar_id($idCategoria);
                $nombreBd=$respuesta['nombre'];
            }

            if ($nombre!=$nombreBd) {
                $verificacion=$c->verificar_existencia_nombre($nombre);
            }


            if (!empty($verificacion)) {
                echo "¡Nombre ya existe!";
            } else {
                if (empty($idCategoria)) {
                    session_start();
                    if ($_SESSION['new_categorias_art']==0) {
                        echo "¡Acción denegada!";
                    } else {
                        // Nuevo
                        $respuesta = $c->nuevo($nombre);
                        echo $respuesta ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
                    }
                } else {
                    session_start();
                    if ($_SESSION['alt_categorias_art']==0) {
                        echo "¡Acción denegada!";
                    } else {
                        // Editar
                        $respuesta = $c->editar($idCategoria, $nombre);
                        echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
                    }
                }
            }
        }
    
            
    break;

    case 'mostrar':
        $respuesta=$c->buscar_id($idCategoria);
        echo json_encode($respuesta);
    break;

    case 'eliminar':
        $respuesta=$c->eliminar($idCategoria);
        echo $respuesta ? "¡Registro eliminado con exito!" : "¡Ocurrió un problema y no se pudo eliminar!";
    break;

    case 'listar':
        session_start();
        $alteracion=$_SESSION['alt_categorias_art'];
        $eliminacion=$_SESSION['del_categorias_art'];
        $respuesta=$c->listar();
        $data = array();

        while ($reg=$respuesta->fetch_object()) {
            $opciones="";
            if ($alteracion==1) {
                $opciones.='<button class="btn btn-warning btn-sm" onclick="mostrar('.$reg->id_categoria.')"><i class="fas fa-pencil-alt"></i></button>';
            }

            if ($eliminacion==1) {
                $opciones.=' <button class="btn btn-danger btn-sm" onclick="eliminar('.$reg->id_categoria.')"><i class="fas fa-trash-alt"></i></button>';
            }

            $data[]=array(
                "0"=>$opciones,
                "1"=>$reg->nombre,
            );
        }

        $results=array(
            "sEcho"=>1, //informacion para el data table
            "iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
            "aaData"=>$data
        );
        echo json_encode($results);
    break;

}