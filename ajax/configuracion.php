<?php
require('../models/Configuracion.php');

$c = new Configuracion();

$nombreEmpresa = isset($_POST['nombreEmpresa']) ? limpiarCadena($_POST['nombreEmpresa']) : "";
$compraMinimaMayorista = isset($_POST['compraMinimaMayorista']) ? limpiarCadena($_POST['compraMinimaMayorista']) : "";
$siguientesComprasMayoristas = isset($_POST['siguientesComprasMayoristas']) ? limpiarCadena($_POST['siguientesComprasMayoristas']) : "";


switch ($_GET['op']) {
    case 'editar':
        if ($nombreEmpresa=="" || $compraMinimaMayorista=="" || $siguientesComprasMayoristas == "") {
            echo "¡Complete los campos obligatorios!";
        } else {
            if (is_uploaded_file($_FILES['logoEmpresa']['tmp_name'])) {
                move_uploaded_file($_FILES['logoEmpresa']['tmp_name'], "../../resources/img/logo-empresa.png");
            }

            $compraMinimaMayorista = str_replace(array('.'), '', $compraMinimaMayorista);
            $compraMinimaMayorista = str_replace(array(','), '.', $compraMinimaMayorista);

            $siguientesComprasMayoristas = str_replace(array('.'), '', $siguientesComprasMayoristas);
            $siguientesComprasMayoristas = str_replace(array(','), '.', $siguientesComprasMayoristas);

            // Editar
            $respuesta = $c->editar($nombreEmpresa, $compraMinimaMayorista, $siguientesComprasMayoristas);
            echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
        }
    break;

    case 'mostrar':
        $respuesta = $c->traer_configuracion();
        echo json_encode($respuesta);
    break;

    
}