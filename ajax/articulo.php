<?php
require '../models/Articulo.php';
require '../models/CategoriaArticulo.php';
include('utilidades/ResizeImage.php');

$a = new Articulo();
$c = new CategoriaArticulo();

$codArticulo = isset($_POST['codArticulo']) ? limpiarCadena($_POST['codArticulo']) : "";
$descripcion = isset($_POST['descripcion']) ? limpiarCadena($_POST['descripcion']) : "";
$precioUnitarioMayorista = isset($_POST['precioUnitarioMayorista']) ? limpiarCadena($_POST['precioUnitarioMayorista']) : "";
$precioUnitarioPublico = isset($_POST['precioUnitarioPublico']) ? limpiarCadena($_POST['precioUnitarioPublico']) : "";
$stockActual = isset($_POST['stockActual']) ? limpiarCadena($_POST['stockActual']) : "";
$estado = isset($_POST['estado']) ? limpiarCadena($_POST['estado']) : "";
$idCategoria = isset($_POST['idCategoria']) ? limpiarCadena($_POST['idCategoria']) : "";
$imagen = isset($_POST['imagen']) ? limpiarCadena($_POST['imagen']) : "";


switch ($_GET['op']) {

    case 'nuevo_editar':
        session_start();

        // Obtenemos el tipo de archivo
        $path = $_FILES['imagen']['name'];
        $tipoImg = pathinfo($path, PATHINFO_EXTENSION);
        if ($tipoImg=="jpeg") {
            $tipoImg="jpg";
        }

        $precioUnitarioMayorista = str_replace(array('.'), '', $precioUnitarioMayorista);
        $precioUnitarioMayorista = str_replace(array(','), '.', $precioUnitarioMayorista);

        $precioUnitarioPublico = str_replace(array('.'), '', $precioUnitarioPublico);
        $precioUnitarioPublico = str_replace(array(','), '.', $precioUnitarioPublico);

        if ($descripcion == "" || $estado == ""  || $idCategoria == "") {
            echo "¡Complete los campos obligatorios!";
        } else {
            $verificacion = null;
            $descripcionBd = "";
            // Si vamos a editar traemos la descripcion de la bd
            if (!empty($codArticulo)) {
                $respuesta = $a->buscar_cod($codArticulo, $_SESSION['idSucursal']);
                $descripcionBd = $respuesta['descripcion'];
            }
            // Verificar existencia descripcion
            if ($descripcion != $descripcionBd) {
                $verificacion = $a->verificar_existencia_descripcion($descripcion);
            }

            if (!empty($verificacion)) {
                echo "¡Descripción ya existe!";
            } else {
                if (empty($codArticulo)) {
                    if ($_SESSION['new_articulos'] == 0) {
                        echo "¡Acción denegada!";
                    } else {
                        // Nuevo
                        $respuesta = $a->nuevo($descripcion, $precioUnitarioMayorista, $precioUnitarioPublico, $stockActual, $estado, $tipoImg, $_SESSION['idSucursal'], $idCategoria);
                        // RETORNO
                        $r = explode(":", $respuesta);
                        $resultadoOperacion = $r[0];
                        $resultadoCod = $r[1];
                        if ($resultadoOperacion == 1) {
                            // Si subimos una imagen hacemos el resize
                            if ($_FILES["imagen"]['name']!="") {
                                $nombreImagen = $resultadoCod;
                                $temp = $_FILES["imagen"]["tmp_name"];
                                $thumb = new Thumbnail($temp);
                                if ($thumb->error) {
                                    echo $thumb->error;
                                } else {
                                    switch ($tipoImg) {
                                        case 'png':
                                            $thumb->resize(500);
                                            $thumb->save_png("../imgArticulos/", $nombreImagen);
                                            echo "¡Registro creado con exito!";
                                        break;
                                        
                                        case 'jpg':
                                            $thumb->resize(500);
                                            $thumb->save_jpg("../imgArticulos/", $nombreImagen);
                                            echo "¡Registro creado con exito!";
                                        break;
                                    }
                                }
                            } else {
                                // si no se cargaron fotos se carga el registro sin hacer resize de imagen
                                echo "¡Registro creado con exito!";
                            }
                        } else {
                            echo "¡Ocurrió un problema y no se pudo crear";
                        }
                    }
                } else {
                    if ($_SESSION['alt_articulos'] == 0) {
                        echo "¡Acción denegada!";
                    } else {
                        // Editar
                        $respuesta = $a->editar($codArticulo, $descripcion, $precioUnitarioMayorista, $precioUnitarioPublico, $stockActual, $estado, $tipoImg, $_SESSION['idSucursal'], $idCategoria);
                        // RETORNO
                        if ($respuesta == 1) {
                           
                            // Si subimos una imagen hacemos el resize
                            if ($_FILES["imagen"]['name']!="") {
                                $nombreImagen = $codArticulo;
                                $temp = $_FILES["imagen"]["tmp_name"];
                                $thumb = new Thumbnail($temp);
                                if ($thumb->error) {
                                    echo $thumb->error;
                                } else {
                                    switch ($tipoImg) {
                                        case 'png':
                                            $thumb->resize(500);
                                            $thumb->save_png("../imgArticulos/", $nombreImagen);
                                            echo "¡Registro editado con exito!";
                                        break;
                                        
                                        case 'jpg':
                                            $thumb->resize(500);
                                            $thumb->save_jpg("../imgArticulos/", $nombreImagen);
                                            echo "¡Registro editado con exito!";
                                        break;
                                    }
                                }
                            } else {
                                // si no se cargaron fotos se carga el registro sin hacer resize de imagen
                                echo "¡Registro editado con exito!";
                            }
                        } else {
                            echo "¡Ocurrió un problema y no se pudo editar";
                        }
                    }
                }
            }
        }
        break;

    case 'cargar_categorias':
        $categorias=$c->listar();
        while ($reg=$categorias->fetch_object()) {
            echo '<option value='.$reg->id_categoria.'>'.$reg->nombre.'</option>';
        }
        break;

    case 'mostrar':
        session_start();
        $respuesta = $a->buscar_cod($codArticulo, $_SESSION['idSucursal']);
        echo json_encode($respuesta);
        break;

    case 'listar':
        session_start();
        $alteracion = $_SESSION['alt_articulos'];
        $respuesta = $a->listar($_SESSION['idSucursal']);
        $data = array();

        while ($reg = $respuesta->fetch_object()) {
            if ($alteracion == 1) {
                $opciones = '<button class="btn btn-warning btn-sm" onclick="mostrar(' . $reg->cod_articulo . ')"><i class="fas fa-pencil-alt"></i></button>';
            } else {
                $opciones = '';
            }

            if ($reg->estado == "ACTIVO") {
                $estado = '<span class="badge badge-success">' . $reg->estado . '</span>';
            } else {
                if ($reg->estado == "INACTIVO") {
                    $estado = '<span class="badge badge-danger">' . $reg->estado . '</span>';
                }
            }

            if ($reg->stock_actual == null) {
                $stock_actual = 0;
            } else {
                $stock_actual = $reg->stock_actual;
            }

            if ($reg->precio_unitario_mayorista <= 9999.99) {
                $precioUnitarioMayorista = "$" . $reg->precio_unitario_mayorista;
            } else {
                $precioUnitarioMayorista = "$" . $reg->precio_unitario_mayorista;
            }

            if ($reg->precio_unitario_publico <= 9999.99) {
                $precioUnitarioPublico = "$" . $reg->precio_unitario_publico;
            } else {
                $precioUnitarioPublico = "$" . $reg->precio_unitario_publico;
            }

            if (!$reg->imagen == "") {
                $imagen = "<a href='../imgArticulos/" . $reg->imagen . "?" . rand() . "' target='_blank'><img class='thumbnail-imagen' src='../imgArticulos/" . $reg->imagen . "?" . rand() . "' width='90px' height='90px'></i></a>";
            } else {
                $imagen = "<img class='thumbnail-imagen' src='../imgArticulos/sin-imagen.jpg?" . rand() . "' width='90px' height='90px'></i>";
            }

            $categoria="";
            if ($reg->id_categoria!=null) {
                $categoria = '<h5><span class="badge badge-info">' . $reg->categoria_nombre . '</span><h5>';
            }
            
            

            $data[] = array(
                "0" => $opciones,
                "1" => $reg->cod_articulo,
                "2" => $estado,
                "3" => $reg->descripcion,
                "4" => $categoria,
                "5" => $imagen,
                "6" => $precioUnitarioMayorista,
                "7" => $precioUnitarioPublico,
                "8" => $stock_actual,
            );
        }

        $results = array(
            "sEcho" => 1, //informacion para el data table
            "iTotalRecords" => count($data), //enbviamos total de registros para al datatable
            "iTotalDisplayRecords" => count($data), //enviamos total de registros a visualizar
            "aaData" => $data,
        );
        echo json_encode($results);
        break;

}