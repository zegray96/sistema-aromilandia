<?php
require('../models/Usuario.php');

$u = new Usuario();

$apellidoNombre = isset($_POST['apellidoNombre']) ? limpiarCadena($_POST['apellidoNombre']) : "";
$usuario = isset($_POST['usuario']) ? limpiarCadena($_POST['usuario']) : "";
$clave = isset($_POST['clave']) ? limpiarCadena($_POST['clave']) : "";

switch ($_GET['op']) {

    case 'modificar_clave':
        session_start();
        $idUsuarioModificar = $_SESSION['idUsuarioSisArom'];
        $claveModificar = isset($_POST['claveModificar']) ? limpiarCadena($_POST['claveModificar']) : "";
        $repetirClaveModificar = isset($_POST['repetirClaveModificar']) ? limpiarCadena($_POST['repetirClaveModificar']) : "";

        if ($claveModificar!=$repetirClaveModificar) {
            echo "¡Claves no coinciden!";
        } else {
            $claveHash=hash("SHA256", $claveModificar);
            $resultado=$u->modificar_clave($idUsuarioModificar, $claveHash);
            if ($resultado==1) {
                echo "¡Clave modificada con exito!";
                $_SESSION['clave']=$claveHash;
            } else {
                echo "¡Ocurrió un problema y no se pudo modificar";
            }
        }
        
    break;

    case 'editar':
        $verificacion=null;
        $usuarioBd="";
        session_start();
        $idUsuario=$_SESSION['idUsuarioSisArom'];

        if ($idUsuario==1) {
            echo "¡No puede editar usuario Super Admin!";
        } else {
            $resultado=$u->buscar_id($idUsuario);
            $usuarioBd=$resultado['usuario'];

            if ($usuario!=$usuarioBd) {
                $verificacion=$u->verificar_existencia_usuario($usuario);
            }

            if (!empty($verificacion)) {
                echo "¡Usuario Ya Existe!";
            } else {
                $resultado=$u->modificar_cuenta($idUsuario, $apellidoNombre, $usuario);
                if ($resultado==1) {
                    echo "¡Datos modificados con exito!";
                    $_SESSION['apellidoNombre']=$apellidoNombre;
                    $_SESSION['usuario']=$usuario;
                } else {
                    echo "¡Ocurrió un problema y no se pudo modificar";
                }
            }
        }
                
        
    break;
    
}