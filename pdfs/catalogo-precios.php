<?php
require('../assets/TCPDF/tcpdf.php');
require('../models/Articulo.php');
require('../models/CategoriaArticulo.php');
require('../models/Configuracion.php');

date_default_timezone_set('America/Argentina/Buenos_Aires');

class MYPDF extends TCPDF
{

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'B', 8);
        // Page number
        $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// remove default header/footer
$pdf->setPrintHeader(false);


// add a page
$pdf->AddPage('P', 'A4');

$pdf->SetTitle('Catalogo de Precios');
$logoEmpresa = '../assets/img/logo-empresa.png';

// Logo
$pdf->Image($logoEmpresa, 60, 5, 15, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);

// CABECERA
$pdf->SetFont('helvetica', 'B', 18);
$pdf->Cell(3);
$pdf->Cell(65, 15, 'Catalogo de Precios ', 0, 0, 'L');

$pdf->SetFont('helvetica', 'I', 14);
$pdf->Cell(15);
$hoy = date('d/m/Y');
$pdf->Cell(20, 16, 'Fecha: '.$hoy, 0, 0, 'L');

$conf = new Configuracion();
$respConf=$conf->traer_configuracion();
$pdf->SetFillColor(232, 232, 232);
$pdf->SetFont('helvetica', 'B', 14);
$pdf->Ln(18);
$pdf->Cell(190, 8, 'MINIMO DE COMPRA MAYORISTA $'.$respConf['compra_minima_mayorista'].'. SIGUIENTES COMPRAS $'.$respConf['siguientes_compras_mayoristas'], 1, 0, 'C', 1);

$pdf->Ln(15);
$pdf->SetFont('helvetica', '', 12);
// output the HTML content
$html="<p><u><strong>NOTA:</strong></u> La mercaderia viaja por cuenta y riesgo del comprador, no se aceptan reclamos.</p>";
$pdf->writeHTML($html, true, false, true, false, '');
// FIN CABECERA

$c = new CategoriaArticulo();
$respuestaCategorias = $c->listar();
$pdf->Ln(10);
$pdf->SetFont('helvetica', 'B', 10);
while ($reg=$respuestaCategorias->fetch_object()) {
    // cabecera
    $pdf->Cell(125, 8, $reg->nombre, 1, 0, 'C', 1);
    $pdf->Cell(32.5, 8, 'P. Publico', 1, 0, 'C', 1);
    $pdf->Cell(32.5, 8, 'P. Mayorista', 1, 0, 'C', 1);
    // fin cabecera
    $pdf->GetY();
    // articulos de categoria
    $a = new Articulo();
    $respuestaArticulos = $a->listar_por_categoria($reg->id_categoria);
    $pdf->Ln(8);
    // SOLO SE MOSTRARAN LOS ARTICULOS ACTIVOS
    while ($regArt=$respuestaArticulos->fetch_object()) {
        if ($regArt->precio_unitario_publico<=9999.99) {
            $precioUnitarioPublico ="$".number_format($regArt->precio_unitario_publico, 2, ',', '');
        } else {
            $precioUnitarioPublico ="$".number_format($regArt->precio_unitario_publico, 2, ',', '.');
        }

        if ($regArt->precio_unitario_mayorista<=9999.99) {
            $precioUnitarioMayorista ="$".number_format($regArt->precio_unitario_mayorista, 2, ',', '');
        } else {
            $precioUnitarioMayorista ="$".number_format($regArt->precio_unitario_mayorista, 2, ',', '.');
        }

        
        if ($regArt->imagen=="") {
            $imagenArt='../imgArticulos/sin-imagen.jpg';
        } else {
            $imagenArt='../imgArticulos/'.$regArt->imagen;
        }
        
        $pdf->Image($imagenArt, 10, $pdf->GetY(), 15, '', '', '', 'T', false, 300, '', false, false, 1, false, false, false);
        $pdf->Rect(10, $pdf->GetY(), 15, 15, 'D');
        $pdf->Cell(110, 15, $regArt->descripcion, 1, 0, 'C', 0);
        $pdf->Cell(32.5, 15, $precioUnitarioPublico, 1, 0, 'C', 0);
        $pdf->Cell(32.5, 15, $precioUnitarioMayorista, 1, 0, 'C', 0);
        $pdf->Ln(15);
    }

    
    // fin articulos de categoria

    $pdf->Ln(7);
}


//Limpiamos cualquier texto anterior
ob_end_clean();

$pdf->Output('catalogo-precios-aromilandia.pdf', 'I');