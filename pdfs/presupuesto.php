<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if (!isset($_SESSION['idUsuarioSisArom'])) {
    header('Location: ../vistas/login');
} else {
    require('../../resources/TCPDF/tcpdf.php');
    require('../models/Presupuesto.php');
    require('../models/Configuracion.php');

    
    if (!isset($_GET['id'])) {
        // si no esta definido el id
        echo '¡Error!';
    } else {
        $p = new Presupuesto();
        $respuesta = $p->buscar_id($_GET['id']);
        if ($respuesta=="") {
            // si el id que le pasamos no existe
            echo "¡No se encontro el presupuesto!";
        } else {
            // Imprimir presupuesto
            // Extend the TCPDF class to create custom Header and Footer
            class MYPDF extends TCPDF
            {
                protected $ultimaPagina = false;
                //Page header
                public function Header()
                {
                    $p = new Presupuesto();
                    $respuesta = $p->buscar_id($_GET['id']);
                    $nroPresupuesto = str_pad($respuesta['nro_presupuesto'], 8, "0", STR_PAD_LEFT);
                    $fechaPresupuesto = $respuesta["fecha_presupuesto"];
                    $apellidoNombre = $respuesta["apellidoNombre"];
                    $dni = $respuesta['dni'];
                    $domicilio = $respuesta['domicilio'];
                    $cuit = $respuesta['cuit'];
                    $telefono= substr($respuesta['telefono'], 0, 25);
                    $localidad= substr($respuesta['localidad'], 0, 30);
                    $provincia= substr($respuesta['provincia'], 0, 30);
    
                    $conf = new Configuracion();
                    $respuesta=$conf->traer_configuracion();
                    $nombreEmpresa = $respuesta['nombre_empresa'];
                    $logoEmpresa = '../../resources/img/logo-empresa.png';
    
                    // Logo
                    $this->Image($logoEmpresa, 23, 5, 25, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    
                    $this->SetFont('helvetica', 'B', 14);
                    $this->Ln(25);
                    // Nombre empresa
                    $this->MultiCell(55, 5, $nombreEmpresa, 0, 'C', false);
    
                    $this->Ln(7);
                    $this->Line(10, $this->y, $this->w - 10, $this->y);
    
                    // DATOS DEL CLIENTE
                    $this->Ln(7);
                    $this->SetFont('helvetica', 'B', 10);
                    // Texto nombre
                    $this->Cell(34, 5, 'Apellido y Nombre: ', 0, 0, 'L');
                    $this->SetFont('helvetica', '', 10);
                    // Nombre cliente
                    $this->Cell(75, 5, $apellidoNombre, 0, 0, 'L');
    
                    $this->Cell(19);
                    $this->SetFont('helvetica', 'B', 10);
                    // Texto dni
                    $this->Cell(8, 5, 'Dni: ', 0, 0, 'L');
                    $this->SetFont('helvetica', '', 10);
                    // Dni cliente
                    $this->Cell(18, 5, $dni, 0, 0, 'L');
    
                    $this->Cell(5);
                    $this->SetFont('helvetica', 'B', 10);
                    // Texto cuit
                    $this->Cell(10, 5, 'Cuit: ', 0, 0, 'L');
                    $this->SetFont('helvetica', '', 10);
                    // Cuit
                    $this->Cell(25, 5, $cuit, 0, 0, 'L');
    
    
    
                    $this->Ln(8);
                    $this->SetFont('helvetica', 'B', 10);
                    // Texto domicilio
                    $this->Cell(19, 5, 'Domicilio: ', 0, 0, 'L');
                    $this->SetFont('helvetica', '', 10);
                    // Domicilio cliente
                    $this->Cell(106, 5, $domicilio, 0, 0, 'L');
    
                    $this->Cell(3);
                    $this->SetFont('helvetica', 'B', 10);
                    // Texto telefono
                    $this->Cell(8, 5, 'Tel.: ', 0, 0, 'L');
                    $this->SetFont('helvetica', '', 10);
                    // Telefono
                    $this->Cell(49, 5, $telefono, 0, 0, 'L');
    
                    $this->Ln(7);
                    $this->SetFont('helvetica', 'B', 10);
                    // Texto telefono
                    $this->Cell(19, 5, 'Localidad: ', 0, 0, 'L');
                    $this->SetFont('helvetica', '', 10);
                    // Telefono
                    $this->Cell(52, 5, $localidad, 0, 0, 'L');
    
                    $this->Cell(10);
                    $this->SetFont('helvetica', 'B', 10);
                    // Texto telefono
                    $this->Cell(19, 5, 'Provincia: ', 0, 0, 'L');
                    $this->SetFont('helvetica', '', 10);
                    // Telefono
                    $this->Cell(52, 5, $provincia, 0, 0, 'L');
    
            
                    // Creamos las celdas para los titulos de cada columna y le asignamos un fondo gris y el tipo de letra
                    $this->SetFillColor(232, 232, 232);
                    $this->SetFont('helvetica', 'B', 10);
                    $this->Ln(10);
                    $this->Cell(20, 6, 'Cantidad', 1, 0, 'C', 1);
                    $this->Cell(110, 6, 'Descripción', 1, 0, 'C', 1);
                    $this->Cell(30, 6, 'Precio Unit.', 1, 0, 'C', 1);
                    $this->Cell(30, 6, 'Subtotal', 1, 0, 'C', 1);
    
                    // Espacio para que vaya el contenido de cliente - detalles - totales
                    $this->SetTopMargin($this->y+6);
    
                    $this->setY(5);
                    $this->Cell(70);
                    $this->SetFont('helvetica', 'B', 18);
                    $this->Cell(45, 5, 'PRESUPUESTO', 0, 0, 'C');
    
                    $this->Ln(8);
                    $this->Cell(70);
                    $this->Cell(45, 5, 'X', 0, 0, 'C');
    
                    $this->SetFont('helvetica', 'B', 9);
                    $this->Ln(8);
                    $this->Cell(70);
                    $this->Cell(45, 5, 'Comprobante No Válido como Factura', 0, 0, 'C');
                    $this->Ln(4);
                    $this->Cell(70);
                    $this->Cell(45, 5, 'Válido por 10 días a partir de la fecha', 0, 0, 'C');
    
                    $this->setY(5);
                    $this->SetFont('helvetica', 'B', 18);
                    $this->Cell(137);
                    // Nro de comprobante
                    $this->Cell(60, 10, '0001'.' - '.$nroPresupuesto, 0, 0, 'C');
    
                    $this->Ln(10);
                    $this->Cell(137);
                    $this->SetFont('helvetica', '', 12);
                    // Fecha comprobante
                    $this->Cell(60, 5, 'Fecha: '.$fechaPresupuesto, 0, 0, 'C');
                }
    
                public function Close()
                {
                    $this->ultimaPagina = true;
                    parent::Close();
                }
    
                // Page footer
                public function Footer()
                {
                    $p = new Presupuesto();
                    $respuesta = $p->buscar_id($_GET['id']);
    
                    if ($respuesta['monto_total']<=9999.99) {
                        $montoTotal = "$".number_format($respuesta['monto_total'], 2, ',', '');
                    } else {
                        $montoTotal = "$".number_format($respuesta['monto_total'], 2, ',', '.');
                    }
            
    
                    if ($respuesta['desc_porcentaje']!=0) {
                        $descuento = $respuesta['desc_porcentaje']."%";
                    } else {
                        if ($respuesta['desc_monto']!=0) {
                            if ($respuesta['desc_monto']<=9999.99) {
                                $descuento = "$".number_format($respuesta['desc_monto'], 2, ',', '');
                            } else {
                                $descuento = "$".number_format($respuesta['desc_monto'], 2, ',', '.');
                            }
                        } else {
                            $descuento = "$0,00";
                        }
                    }
    
            
                    if ($this->ultimaPagina) {
                        // Position at 15 mm from bottom
                        $this->SetY(-55);
    
                        $this->SetFont('helvetica', 'B', 14);
    
                
                        if ($descuento!='$0,00') {
                            $this->Cell(130);
                            if ($respuesta['desc_porcentaje']!=0) {
                                $totalSinDesc=(100*$respuesta['monto_total'])/(100-$respuesta['desc_porcentaje']);
                            } else {
                                $totalSinDesc=$respuesta['monto_total']+$respuesta['desc_monto'];
                            }
    
                            if ($totalSinDesc<=9999.99) {
                                $totalSinDesc="$".number_format($totalSinDesc, 2, ',', '');
                            } else {
                                $totalSinDesc="$".number_format($totalSinDesc, 2, ',', '.');
                            }
    
                            $this->Cell(60, 6, 'Total sin descuento: '.$totalSinDesc, 0, 0, 'R');
                        }
                        
                        $this->Ln(8);
                        $this->Cell(130);
                        $this->Cell(60, 6, 'Descuento: '.$descuento, 0, 0, 'R');
                
    
                        $this->SetFont('helvetica', 'B', 20);
                        $this->Ln(8);
                        $this->Cell(130);
                        $this->Cell(60, 6, 'TOTAL: '.$montoTotal, 0, 0, 'R');

                        $this->Ln(15);
                        $this->SetFont('helvetica', '', 12);
                        // output the HTML content
                        $html="<p><u><strong>NOTA:</strong></u> La mercaderia viaja por cuenta y riesgo del comprador, no se aceptan reclamos.</p>";
                        $this->writeHTML($html, true, false, true, false, '');

    
                        $this->Ln(5);
                        $this->Line(10, $this->y, $this->w - 10, $this->y);
                        // Set font
                        $this->SetFont('helvetica', 'B', 8);
                        // Page number
                        $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
                    } else {
                        // Position at 15 mm from bottom
                        $this->SetY(-15);
                        // Set font
                        $this->SetFont('helvetica', 'B', 8);
                        // Page number
                        $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
                    }
                }
            }
    
            // create new PDF document
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    
            $pdf->SetTitle('Presupuesto');
            // Margin bottom pagina
            $pdf->SetAutoPageBreak(true, 50);
    
            $p = new Presupuesto();
            $respuesta = $p->buscar_id($_GET['id']);
            $nroPresupuesto = $respuesta['nro_presupuesto'];
    
            // add a page
            $pdf->AddPage('P', 'A4');
    
     
    
            $pdf->SetFont('helvetica', 'B', 10);
    
            // Comenzamos a crear las filas de los registros segun la consulta mysql
            $respuesta = $p->listar_detalles($_GET['id']);
    
            while ($reg=$respuesta->fetch_object()) {
                $cantidad = $reg->cantidad;
                $descripcion= substr($reg->descripcion, 0, 60);
                if ($reg->precio_unitario<=9999.99) {
                    $precioUnitario="$".number_format($reg->precio_unitario, 2, ',', '');
                } else {
                    $precioUnitario="$".number_format($reg->precio_unitario, 2, ',', '.');
                }
    
                if ($reg->subtotal<=9999.99) {
                    $subtotal="$".number_format($reg->subtotal, 2, ',', '');
                } else {
                    $subtotal="$".number_format($reg->subtotal, 2, ',', '.');
                }
    
       
                $pdf->Cell(20, 5, $cantidad, 1, 0, 'C');
                $pdf->Cell(110, 5, ' '.$descripcion, 1, 0, 'L');
        
                $pdf->Cell(30, 5, $precioUnitario, 1, 0, 'C');
                $pdf->Cell(30, 5, $subtotal, 1, 0, 'C');
    
                $pdf->Ln();
            }
            
            //Limpiamos cualquier texto anterior
            ob_end_clean();

            $pdf->Output('presupuesto-'.$nroPresupuesto.'.pdf', 'I');

            // Fin imprimir presupuesto
        }
    }
}

ob_end_flush(); //libera el espacio del buffer