﻿<?php
//Servidor de base de datos
define("DB_HOST", "localhost");

//Nombre de la base de datos
define("DB_NAME", "aromilandia");

//Usuario de la base de datos
define("DB_USERNAME", "root");

//Clave del usuario de la base de datos
define("DB_PASSWORD", "");

//Codificacion de los caracteres
define("DB_ENCODE", "utf8");

//Nombre del proyecto
define("PRO_NOMBRE", "SistemaAromilandia");

//Version del proyecto
define("VERSION", "3.3");

//Dominio
define("DOMAIN", "");