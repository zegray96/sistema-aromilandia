<?php
require('../config/connection.php');
class Rol
{
    public function __construct()
    {
    }

    public function listar()
    {
        $sql="SELECT * FROM roles";
        return ejecutarConsulta($sql);
    }

    public function nuevo(
        $nombre,
        $vAcceso,
        $vClientes,
        $vArticulos,
        $vCategoriasArt,
        $vPresupuestos,
        $vConfiguracion,
        $vMetodosPago,
        $altAcceso,
        $altClientes,
        $altArticulos,
        $altCategoriasArt,
        $altPresupuestos,
        $altMetodosPago,
        $newAcceso,
        $newClientes,
        $newArticulos,
        $newCategoriasArt,
        $newPresupuestos,
        $newMetodosPago,
        $delPresupuestos,
        $delCategoriasArt,
        $delMetodosPago
    ) {
        $sql="INSERT INTO roles (nombre, v_acceso, v_clientes, v_articulos, v_categorias_art, v_presupuestos, v_configuracion, v_metodos_pago, alt_acceso, alt_clientes, alt_articulos, alt_categorias_art, alt_presupuestos, alt_metodos_pago, new_acceso, new_clientes, new_articulos, new_categorias_art, new_presupuestos, new_metodos_pago, del_presupuestos, del_categorias_art, del_metodos_pago) VALUES 
        ('$nombre','$vAcceso','$vClientes','$vArticulos','$vCategoriasArt','$vPresupuestos','$vConfiguracion','$vMetodosPago','$altAcceso','$altClientes','$altArticulos','$altCategoriasArt','$altPresupuestos','$altMetodosPago','$newAcceso','$newClientes','$newArticulos','$newCategoriasArt','$newPresupuestos','$newMetodosPago','$delPresupuestos','$delCategoriasArt','$delMetodosPago')";
        return ejecutarConsulta($sql);
    }

    public function editar(
        $idRol,
        $nombre,
        $vAcceso,
        $vClientes,
        $vArticulos,
        $vCategoriasArt,
        $vPresupuestos,
        $vConfiguracion,
        $vMetodosPago,
        $altAcceso,
        $altClientes,
        $altArticulos,
        $altCategoriasArt,
        $altPresupuestos,
        $altMetodosPago,
        $newAcceso,
        $newClientes,
        $newArticulos,
        $newCategoriasArt,
        $newPresupuestos,
        $newMetodosPago,
        $delPresupuestos,
        $delCategoriasArt,
        $delMetodosPago
    ) {
        $sql="UPDATE roles SET nombre = '$nombre', v_acceso = '$vAcceso', v_clientes = '$vClientes', v_articulos='$vArticulos', v_categorias_art='$vCategoriasArt', v_presupuestos = '$vPresupuestos', v_configuracion='$vConfiguracion', v_metodos_pago='$vMetodosPago', alt_acceso='$altAcceso', alt_clientes='$altClientes', alt_articulos='$altArticulos', alt_categorias_art='$altCategoriasArt', alt_presupuestos='$altPresupuestos', alt_metodos_pago='$altMetodosPago', 
        new_acceso='$newAcceso', new_clientes='$newClientes', new_articulos='$newArticulos', new_categorias_art='$newCategoriasArt', new_presupuestos='$newPresupuestos', new_metodos_pago='$newMetodosPago', del_presupuestos='$delPresupuestos', del_categorias_art='$delCategoriasArt', del_metodos_pago='$delMetodosPago' WHERE id_rol = '$idRol'";
        return ejecutarConsulta($sql);
    }

    public function buscar_id($idRol)
    {
        $sql="SELECT * FROM roles WHERE id_rol='$idRol'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function verificar_existencia_nombre($nombre)
    {
        $sql="SELECT * FROM roles WHERE nombre='$nombre'";
        return ejecutarConsultaSimpleFila($sql);
    }
}
