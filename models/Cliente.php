<?php
require('../config/connection.php');

class Cliente
{
    public function __construct()
    {
    }

    public function listar()
    {
        $sql="SELECT * FROM clientes";
        return ejecutarConsulta($sql);
    }

    public function listar_activos()
    {
        $sql="SELECT * FROM clientes WHERE estado='ACTIVO'";
        return ejecutarConsulta($sql);
    }

    public function nuevo($cuit, $apellidoNombre, $dni, $domicilio, $telefono, $localidad, $provincia, $estado)
    {
        $sql="INSERT INTO clientes (cuit, apellido_nombre, dni, domicilio, telefono, localidad, provincia, estado) VALUES ('$cuit', '$apellidoNombre', '$dni', '$domicilio', '$telefono', '$localidad', '$provincia', '$estado')";
        return ejecutarConsulta($sql);
    }

    public function nuevo_en_presupuesto($cuit, $apellidoNombre, $dni, $domicilio, $telefono, $localidad, $provincia, $estado)
    {
        $sql="INSERT INTO clientes (cuit, apellido_nombre, dni, domicilio, telefono, localidad, provincia, estado) VALUES ('$cuit', '$apellidoNombre', '$dni', '$domicilio', '$telefono', '$localidad', '$provincia', '$estado')";
        return ejecutarConsulta_retornarID($sql);
    }

    public function editar($codCliente, $cuit, $apellidoNombre, $dni, $domicilio, $telefono, $localidad, $provincia, $estado)
    {
        $sql="UPDATE clientes SET cuit='$cuit', apellido_nombre = '$apellidoNombre', dni = '$dni', domicilio='$domicilio', telefono = '$telefono', localidad='$localidad', provincia='$provincia', estado='$estado' WHERE cod_cliente = '$codCliente'";
        return ejecutarConsulta($sql);
    }


    public function buscar_cod($codCliente)
    {
        $sql="SELECT * FROM clientes WHERE cod_cliente='$codCliente'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function buscar_cod_manual($codCliente)
    {
        $sql="SELECT * FROM clientes WHERE cod_cliente='$codCliente' AND estado='ACTIVO'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function verificar_existencia_dni($dni)
    {
        $sql="SELECT * FROM clientes WHERE dni='$dni'";
        return ejecutarConsultaSimpleFila($sql);
    }
}