<?php
require('../config/connection.php');
class MetodoPago
{
    public function __construct()
    {
    }

    public function listar()
    {
        $sql="SELECT * FROM metodos_pago";
        return ejecutarConsulta($sql);
    }

    public function nuevo($nombre)
    {
        $sql="INSERT INTO metodos_pago (nombre) VALUES ('$nombre')";
        return ejecutarConsulta($sql);
    }

    public function editar($idMetodoPago, $nombre)
    {
        $sql="UPDATE metodos_pago SET nombre = '$nombre' WHERE id_metodo_pago = '$idMetodoPago'";
        return ejecutarConsulta($sql);
    }

    public function eliminar($idMetodoPago)
    {
        $sql="DELETE FROM metodos_pago WHERE id_metodo_pago = '$idMetodoPago'";
        return ejecutarConsulta($sql);
    }

    public function buscar_en_presupuestos_metodos_pago($idMetodoPago)
    {
        $sql="SELECT COUNT(id_metodo_pago) as cantidad FROM presupuestos_metodos_pago WHERE id_metodo_pago='$idMetodoPago'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function buscar_id($idMetodoPago)
    {
        $sql="SELECT * FROM metodos_pago WHERE id_metodo_pago='$idMetodoPago'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function verificar_existencia_nombre($nombre)
    {
        $sql="SELECT * FROM metodos_pago WHERE nombre='$nombre'";
        return ejecutarConsultaSimpleFila($sql);
    }
}