<?php
require '../config/connection.php';

class Articulo
{
    public function __construct()
    {
    }

    public function listar($idSucursal)
    {
        $sql = "SELECT a.cod_articulo, a.descripcion, a.precio_unitario_mayorista, a.precio_unitario_publico, a.estado, a.imagen, art_suc.stock_actual as stock_actual, a.id_categoria as id_categoria, c.nombre as categoria_nombre FROM articulos a
		LEFT JOIN articulos_sucursales art_suc ON art_suc.cod_articulo = a.cod_articulo AND art_suc.id_sucursal = '$idSucursal'
        LEFT JOIN categorias_art c ON c.id_categoria = a.id_categoria";
        return ejecutarConsulta($sql);
    }

    public function listar_activos($idSucursal)
    {
        $sql = "SELECT a.cod_articulo, a.descripcion, a.precio_unitario_mayorista, a.precio_unitario_publico, a.estado, a.imagen, art_suc.stock_actual as stock_actual FROM articulos a
		LEFT JOIN articulos_sucursales art_suc ON art_suc.cod_articulo = a.cod_articulo AND art_suc.id_sucursal = '$idSucursal'
		WHERE a.estado='ACTIVO'";
        return ejecutarConsulta($sql);
    }

    public function listar_por_categoria($idCategoria)
    {
        $sql = "SELECT * FROM articulos WHERE id_categoria='$idCategoria' AND estado='ACTIVO'
        ORDER BY descripcion ASC";
        return ejecutarConsulta($sql);
    }


    public function nuevo($descripcion, $precioUnitarioMayorista, $precioUnitarioPublico, $stockActual, $estado, $tipoImg, $idSucursal, $idCategoria)
    {
        $sql = "INSERT INTO articulos (descripcion, precio_unitario_mayorista, precio_unitario_publico, estado, id_categoria) VALUES ('$descripcion', '$precioUnitarioMayorista', '$precioUnitarioPublico', '$estado', '$idCategoria')";
        $codArticulo = ejecutarConsulta_retornarID($sql);

        if ($tipoImg != "") {
            // insertamos el nombre de la imagen
            $nombreImagen = $codArticulo . "." . $tipoImg;
            $sql = "UPDATE articulos SET imagen = '$nombreImagen' WHERE cod_articulo = '$codArticulo'";
            ejecutarConsulta($sql);
        }


        $sql = "INSERT INTO articulos_sucursales (cod_articulo, id_sucursal, stock_actual) VALUES ('$codArticulo', '$idSucursal', '$stockActual')";

        return ejecutarConsulta($sql) . ":" . $codArticulo;
    }

    public function editar($codArticulo, $descripcion, $precioUnitarioMayorista, $precioUnitarioPublico, $stockActual, $estado, $tipoImg, $idSucursal, $idCategoria)
    {
        $respuesta = self::buscar_cod($codArticulo, $idSucursal);
        $imagenAnterior = $respuesta['imagen'];

        $sql = "UPDATE articulos SET descripcion = '$descripcion', precio_unitario_mayorista = '$precioUnitarioMayorista', precio_unitario_publico='$precioUnitarioPublico', estado='$estado', id_categoria='$idCategoria' WHERE cod_articulo = '$codArticulo'";
        ejecutarConsulta($sql);

        if ($tipoImg != "") {
            // insertamos el nombre de la imagen
            $nombreImagen = $codArticulo . "." . $tipoImg;
            $sql = "UPDATE articulos SET imagen = '$nombreImagen' WHERE cod_articulo = '$codArticulo'";
            ejecutarConsulta($sql);
            // Si tenemos un archivo para este articulo, lo borramos, para luego crearlo de nuevo
            if ($imagenAnterior != "") {
                $ubicacion = '../../imgArticulos/' . $imagenAnterior;
                //borra el archivo viejo en la carpeta
                unlink($ubicacion);
            }
        }

        // traemos el registro en articulos_sucursales si existe
        $sql = "SELECT * FROM articulos_sucursales WHERE cod_articulo='$codArticulo' AND id_sucursal='$idSucursal'";
        $resultado = ejecutarConsulta($sql);

        if (mysqli_num_rows($resultado) > 0) {
            // editamos tambien en articulos sucursales
            $sql = "UPDATE articulos_sucursales SET stock_actual='$stockActual' WHERE cod_articulo='$codArticulo' AND id_sucursal='$idSucursal'";
            return ejecutarConsulta($sql);
        } else {
            // creamos el registro en articulos_sucursales
            $sql = "INSERT INTO articulos_sucursales (cod_articulo, id_sucursal, stock_actual) VALUES ('$codArticulo', '$idSucursal', '$stockActual')";
            return ejecutarConsulta($sql);
        }
    }

    public function buscar_cod($codArticulo, $idSucursal)
    {
        $sql = "SELECT a.cod_articulo, a.descripcion, a.precio_unitario_mayorista, a.precio_unitario_publico, a.estado, a.imagen, art_suc.stock_actual as stock_actual, a.id_categoria FROM articulos a
		LEFT JOIN articulos_sucursales art_suc ON art_suc.cod_articulo = a.cod_articulo AND art_suc.id_sucursal = '$idSucursal'
		WHERE a.cod_articulo='$codArticulo'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function buscar_img_articulo($codArticulo)
    {
        $sql = "SELECT imagen FROM articulos
		WHERE cod_articulo='$codArticulo'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function verificar_existencia_descripcion($descripcion)
    {
        $sql = "SELECT * FROM articulos WHERE descripcion='$descripcion'";
        return ejecutarConsultaSimpleFila($sql);
    }
}