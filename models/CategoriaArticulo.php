<?php
require('../config/connection.php');
class CategoriaArticulo
{
    public function __construct()
    {
    }

    public function listar()
    {
        $sql="SELECT * FROM categorias_art";
        return ejecutarConsulta($sql);
    }

    public function nuevo($nombre)
    {
        $sql="INSERT INTO categorias_art (nombre) VALUES ('$nombre')";
        return ejecutarConsulta($sql);
    }

    public function editar($idCategoria, $nombre)
    {
        $sql="UPDATE categorias_art SET nombre = '$nombre' WHERE id_categoria = '$idCategoria'";
        return ejecutarConsulta($sql);
    }

    public function eliminar($idCategoria)
    {
        $sql="DELETE FROM categorias_art WHERE id_categoria = '$idCategoria'";
        return ejecutarConsulta($sql);
    }

    public function buscar_id($idCategoria)
    {
        $sql="SELECT * FROM categorias_art WHERE id_categoria='$idCategoria'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function verificar_existencia_nombre($nombre)
    {
        $sql="SELECT * FROM categorias_art WHERE nombre='$nombre'";
        return ejecutarConsultaSimpleFila($sql);
    }
}