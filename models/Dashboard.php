<?php
require('../config/connection.php');

class Dashboard
{
    public function __construct()
    {
    }

    public function listar_sucursales()
    {
        $sql="SELECT * FROM sucursales";
        return ejecutarConsulta($sql);
    }

    public function calcular_total($idSucursal, $fechaIni, $fechaFin)
    {
        $sql="SELECT p.id_presupuesto, p.nro_presupuesto, p.fecha_presupuesto, TIME_FORMAT(p.hora_presupuesto, '%H:%i') as hora_presupuesto, p.cod_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, p.monto_total, p.id_sucursal, s.nombre as sucursal, p.estado, p.desc_porcentaje, p.desc_monto FROM presupuestos p
        INNER JOIN clientes c ON c.cod_cliente=p.cod_cliente
        INNER JOIN sucursales s ON s.id_sucursal=p.id_sucursal
        WHERE p.id_sucursal='$idSucursal' AND p.fecha_presupuesto BETWEEN '$fechaIni' AND '$fechaFin'";
        return ejecutarConsulta($sql);
    }

    public function traer_suma_total($idSucursal, $fechaIni, $fechaFin)
    {
        $sql="SELECT SUM(monto_total) as suma FROM presupuestos WHERE id_sucursal='$idSucursal' AND fecha_presupuesto BETWEEN '$fechaIni' AND '$fechaFin' AND estado='PAGADO'";
        return ejecutarConsultaSimpleFila($sql);
    }
}