<?php
require('../config/connection.php');

class Usuario
{
    public function __construct()
    {
    }

    public function verificar_login($usuario, $clave)
    {
        $sql="SELECT u.id_usuario, u.apellido_nombre, u.usuario, u.clave, u.id_rol, r.nombre as rol, u.id_sucursal, s.nombre as sucursal, u.estado, r.v_acceso as v_acceso,
		r.v_clientes as v_clientes, r.v_presupuestos as v_presupuestos, r.v_articulos as v_articulos, r.v_categorias_art as v_categorias_art, r.v_configuracion as v_configuracion, r.v_metodos_pago as v_metodos_pago,
        r.alt_acceso as alt_acceso, r.alt_clientes as alt_clientes, r.alt_presupuestos as alt_presupuestos, r.alt_articulos as alt_articulos, r.alt_categorias_art as alt_categorias_art, r.alt_metodos_pago as alt_metodos_pago,
        r.new_acceso as new_acceso, r.new_clientes as new_clientes, r.new_presupuestos as new_presupuestos, r.new_articulos as new_articulos, r.new_categorias_art as new_categorias_art, r.new_metodos_pago as new_metodos_pago,
        r.del_presupuestos as del_presupuestos, r.del_categorias_art as del_categorias_art, r.del_metodos_pago as del_metodos_pago FROM usuarios u
		INNER JOIN roles r ON r.id_rol = u.id_rol
		INNER JOIN sucursales s ON s.id_sucursal = u.id_sucursal
		WHERE u.usuario = '$usuario' AND u.clave = '$clave' AND u.estado='ACTIVO'";
        return ejecutarConsulta($sql);
    }

    public function nuevo($apellidoNombre, $usuario, $clave, $idRol, $idSucursal, $estado)
    {
        $sql="INSERT INTO usuarios (apellido_nombre, usuario, clave, id_rol, id_sucursal, estado) VALUES ('$apellidoNombre', '$usuario', '$clave', '$idRol', '$idSucursal', '$estado')";
        return ejecutarConsulta($sql);
    }

    public function editar($idUsuario, $apellidoNombre, $usuario, $idRol, $idSucursal, $estado)
    {
        $sql="UPDATE usuarios SET apellido_nombre='$apellidoNombre', usuario='$usuario', id_rol='$idRol', id_sucursal='$idSucursal', estado='$estado' WHERE id_usuario='$idUsuario'";
        return ejecutarConsulta($sql);
    }

    public function modificar_cuenta($idUsuario, $apellidoNombre, $usuario)
    {
        $sql="UPDATE usuarios SET apellido_nombre='$apellidoNombre', usuario='$usuario' WHERE id_usuario='$idUsuario'";
        return ejecutarConsulta($sql);
    }

    public function modificar_clave($idUsuario, $clave)
    {
        $sql="UPDATE usuarios SET clave='$clave' WHERE id_usuario='$idUsuario'";
        return ejecutarConsulta($sql);
    }

    public function listar()
    {
        $sql="SELECT u.id_usuario, u.apellido_nombre, u.usuario, u.clave, r.nombre as rol, s.nombre as sucursal, u.estado FROM usuarios u
		INNER JOIN roles r ON r.id_rol = u.id_rol
		INNER JOIN sucursales s ON s.id_sucursal = u.id_sucursal";
        return ejecutarConsulta($sql);
    }

    public function buscar_id($idUsuario)
    {
        $sql="SELECT * FROM usuarios WHERE id_usuario='$idUsuario'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar_sucursales()
    {
        $sql="SELECT * FROM sucursales";
        return ejecutarConsulta($sql);
    }

    public function verificar_existencia_usuario($usuario)
    {
        $sql="SELECT * FROM usuarios WHERE usuario='$usuario'";
        return ejecutarConsultaSimpleFila($sql);
    }
}