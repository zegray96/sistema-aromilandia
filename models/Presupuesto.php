<?php
require('../config/connection.php');

class Presupuesto
{
    public function __construct()
    {
    }

    public function listar()
    {
        $sql="SELECT p.id_presupuesto, p.nro_presupuesto, p.fecha_presupuesto, p.hora_presupuesto, p.cod_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, p.monto_total, p.id_sucursal, s.nombre as sucursal, p.estado, p.desc_porcentaje, p.desc_monto FROM presupuestos p
        INNER JOIN clientes c ON c.cod_cliente=p.cod_cliente
        INNER JOIN sucursales s ON s.id_sucursal=p.id_sucursal";
        return ejecutarConsulta($sql);
    }

    public function listar_hoy()
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $hoy = date("Y-m-d");
        $sql="SELECT p.id_presupuesto, p.nro_presupuesto, p.fecha_presupuesto, p.hora_presupuesto, p.cod_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, p.monto_total, p.id_sucursal, s.nombre as sucursal, p.estado, p.desc_porcentaje, p.desc_monto FROM presupuestos p
        INNER JOIN clientes c ON c.cod_cliente=p.cod_cliente
        INNER JOIN sucursales s ON s.id_sucursal=p.id_sucursal
        WHERE p.fecha_presupuesto = '$hoy'";
        return ejecutarConsulta($sql);
    }

    public function listar_por_sucursal($idSucursal, $fechaIni, $fechaFin)
    {
        $sql="SELECT p.id_presupuesto, p.nro_presupuesto, p.fecha_presupuesto, p.hora_presupuesto, p.cod_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, p.monto_total, p.id_sucursal, s.nombre as sucursal, p.estado, p.desc_porcentaje, p.desc_monto FROM presupuestos p
        INNER JOIN clientes c ON c.cod_cliente=p.cod_cliente
        INNER JOIN sucursales s ON s.id_sucursal=p.id_sucursal
        WHERE p.id_sucursal='$idSucursal' AND p.fecha_presupuesto BETWEEN '$fechaIni' AND '$fechaFin'";
        return ejecutarConsulta($sql);
    }

    public function listar_por_fecha($fechaIni, $fechaFin)
    {
        $sql="SELECT p.id_presupuesto, p.nro_presupuesto, p.fecha_presupuesto, p.hora_presupuesto, p.cod_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, p.monto_total, p.id_sucursal, s.nombre as sucursal, p.estado, p.desc_porcentaje, p.desc_monto FROM presupuestos p
        INNER JOIN clientes c ON c.cod_cliente=p.cod_cliente
        INNER JOIN sucursales s ON s.id_sucursal=p.id_sucursal
        WHERE p.fecha_presupuesto>='$fechaIni' AND p.fecha_presupuesto<='$fechaFin'";
        return ejecutarConsulta($sql);
    }

    public function listar_presupuestos_por_cliente($codCliente)
    {
        $sql="SELECT p.id_presupuesto, p.fecha_presupuesto, p.hora_presupuesto, p.nro_presupuesto, p.monto_total, c.apellido_nombre as apellidoNombre, c.dni as dni, p.id_sucursal, s.nombre as sucursal, p.estado, p.desc_porcentaje, p.desc_monto
		FROM presupuestos p
		INNER JOIN clientes c ON c.cod_cliente = p.cod_cliente
		INNER JOIN sucursales s ON s.id_sucursal=p.id_sucursal
		WHERE p.cod_cliente='$codCliente'";
        return ejecutarConsulta($sql);
    }

    public function nuevo($fechaPresupuesto, $codCliente, $descMonto, $descPorcentaje, $montoTotal, $idSucursal, $codArticulo, $descripcion, $precioUnitario, $cantidad, $subtotal, $estadoPresu, $idMetodoPago, $montoMetodoPago)
    {
        $respuesta=self::generar_nro_presupuesto();
        if ($respuesta==null) {
            $nroPresupuesto=1;
        } else {
            $nroPresupuesto=$respuesta['nro_presupuesto'];
        }

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $horaPresupuesto = date("H:i:s");

        $sql="INSERT INTO presupuestos (nro_presupuesto, fecha_presupuesto, hora_presupuesto, cod_cliente, desc_monto, desc_porcentaje, monto_total, id_sucursal, estado) VALUES ('$nroPresupuesto', '$fechaPresupuesto', '$horaPresupuesto','$codCliente', '$descMonto', '$descPorcentaje','$montoTotal', '$idSucursal', '$estadoPresu')";
        $idPresupuesto=ejecutarConsulta_retornarID($sql);



        //crear detalles
        $i=0;
        $sw=true;
        while ($i<count($codArticulo)) {
            $sql="INSERT INTO detalles_presupuestos (cod_articulo, descripcion, precio_unitario, cantidad, subtotal, id_presupuesto) VALUES ('$codArticulo[$i]', '$descripcion[$i]', '$precioUnitario[$i]','$cantidad[$i]', '$subtotal[$i]', '$idPresupuesto')";
            ejecutarConsulta($sql) or $sw=false;
            $i++;
        }

        //actualizar stock
        $i=0;
        $sw=true;
        while ($i<count($codArticulo)) {
            $sql="UPDATE articulos_sucursales SET stock_actual=stock_actual-'$cantidad[$i]' WHERE cod_articulo='$codArticulo[$i]' AND id_sucursal='$idSucursal'";
            ejecutarConsulta($sql) or $sw=false;
            $i++;
        }

        //poner en 0 los stock negativos
        $i=0;
        $sw=true;
        while ($i<count($codArticulo)) {
            $sql="UPDATE articulos_sucursales SET stock_actual=0 WHERE cod_articulo='$codArticulo[$i]' AND id_sucursal='$idSucursal' AND stock_actual<0";
            ejecutarConsulta($sql) or $sw=false;
            $i++;
        }

        //crear metodos de pagos x presupuesto
        if ($idMetodoPago!="") {
            $i=0;
            $sw=true;
            while ($i<count($idMetodoPago)) {
                $sql="INSERT INTO presupuestos_metodos_pago (id_metodo_pago, id_presupuesto, monto) VALUES ('$idMetodoPago[$i]', '$idPresupuesto', '$montoMetodoPago[$i]')";
                ejecutarConsulta($sql) or $sw=false;
                $i++;
            }
        }
       
        



        return $sw.":".$idPresupuesto;
    }

    public function editar($idPresupuesto, $fechaPresupuesto, $codCliente, $descMonto, $descPorcentaje, $montoTotal, $idSucursal, $codArticulo, $descripcion, $precioUnitario, $cantidad, $subtotal, $estadoPresu, $idMetodoPago, $montoMetodoPago)
    {
        $sql="UPDATE presupuestos SET fecha_presupuesto='$fechaPresupuesto', cod_cliente='$codCliente', desc_monto='$descMonto', desc_porcentaje='$descPorcentaje', monto_total='$montoTotal', estado='$estadoPresu' WHERE id_presupuesto='$idPresupuesto'";
        ejecutarConsulta($sql);

        //volver a stock detalles
        $sw=true;
        $resultado=self::listar_detalles($idPresupuesto);
        while ($reg=$resultado->fetch_object()) {
            $cant=$reg->cantidad;
            $cod=$reg->cod_articulo;
            $sql="UPDATE articulos_sucursales SET stock_actual=stock_actual+'$cant' WHERE cod_articulo='$cod' AND id_sucursal='$idSucursal'";
            ejecutarConsulta($sql) or $sw=false;
        }

        // eliminamos detalles de presupuestos
        $sql="DELETE FROM detalles_presupuestos WHERE id_presupuesto='$idPresupuesto'";
        ejecutarConsulta($sql);



        //crear detalles
        $i=0;
        $sw=true;
        while ($i<count($codArticulo)) {
            $sql="INSERT INTO detalles_presupuestos (cod_articulo, descripcion, precio_unitario, cantidad, subtotal, id_presupuesto) VALUES ('$codArticulo[$i]', '$descripcion[$i]', '$precioUnitario[$i]','$cantidad[$i]', '$subtotal[$i]', '$idPresupuesto')";
            ejecutarConsulta($sql) or $sw=false;
            $i++;
        }

        //actualizar stock
        $i=0;
        $sw=true;
        while ($i<count($codArticulo)) {
            $sql="UPDATE articulos_sucursales SET stock_actual=stock_actual-'$cantidad[$i]' WHERE cod_articulo='$codArticulo[$i]' AND id_sucursal='$idSucursal'";
            ejecutarConsulta($sql) or $sw=false;
            $i++;
        }

        //poner en 0 los stock negativos
        $i=0;
        $sw=true;
        while ($i<count($codArticulo)) {
            $sql="UPDATE articulos_sucursales SET stock_actual=0 WHERE cod_articulo='$codArticulo[$i]' AND id_sucursal='$idSucursal' AND stock_actual<0";
            ejecutarConsulta($sql) or $sw=false;
            $i++;
        }

        // eliminamos presupuestos x metodos de pago
        $sql="DELETE FROM presupuestos_metodos_pago WHERE id_presupuesto='$idPresupuesto'";
        ejecutarConsulta($sql);

        //crear metodos de pagos x presupuesto
        if ($idMetodoPago!="") {
            $i=0;
            $sw=true;
            while ($i<count($idMetodoPago)) {
                $sql="INSERT INTO presupuestos_metodos_pago (id_metodo_pago, id_presupuesto, monto) VALUES ('$idMetodoPago[$i]', '$idPresupuesto', '$montoMetodoPago[$i]')";
                ejecutarConsulta($sql) or $sw=false;
                $i++;
            }
        }
       

        



        return $sw.":".$idPresupuesto;
    }


    public function buscar_id($idPresupuesto)
    {
        $sql="SELECT p.id_presupuesto, p.nro_presupuesto, CONCAT(LPAD(DAY(p.fecha_presupuesto),2,'0'),'/', LPAD(MONTH(p.fecha_presupuesto),2,'0'), '/', YEAR(p.fecha_presupuesto)) as fecha_presupuesto, TIME_FORMAT(p.hora_presupuesto, '%H:%i') as hora_presupuesto, p.cod_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, c.domicilio as domicilio, c.cuit as cuit, c.telefono as telefono, c.localidad as localidad, c.provincia as provincia, p.desc_porcentaje, p.desc_monto, p.monto_total, p.id_sucursal, p.estado FROM presupuestos p
        INNER JOIN clientes c ON c.cod_cliente=p.cod_cliente
        INNER JOIN sucursales s ON s.id_sucursal=p.id_sucursal
        WHERE p.id_presupuesto='$idPresupuesto'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar_detalles($idPresupuesto)
    {
        $sql="SELECT dp.cod_articulo, dp.descripcion, dp.precio_unitario, dp.cantidad, dp.subtotal FROM detalles_presupuestos dp WHERE dp.id_presupuesto='$idPresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function listar_metodos_pago($idPresupuesto)
    {
        $sql="SELECT  pmp.id_metodo_pago, mp.nombre as metodo_pago_nombre, pmp.monto FROM presupuestos_metodos_pago as pmp 
		LEFT JOIN metodos_pago as mp ON mp.id_metodo_pago = pmp.id_metodo_pago
		WHERE pmp.id_presupuesto='$idPresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function generar_nro_presupuesto()
    {
        $sql="SELECT nro_presupuesto+1 as nro_presupuesto FROM presupuestos ORDER BY nro_presupuesto DESC LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function eliminar($idPresupuesto, $idSucursal)
    {

        //volver a stock detalles
        $sw=true;
        $resultado=self::listar_detalles($idPresupuesto);
        while ($reg=$resultado->fetch_object()) {
            $cant=$reg->cantidad;
            $cod=$reg->cod_articulo;
            $sql="UPDATE articulos_sucursales SET stock_actual=stock_actual+'$cant' WHERE cod_articulo='$cod' AND id_sucursal='$idSucursal'";
            ejecutarConsulta($sql) or $sw=false;
        }


        $sql="DELETE FROM presupuestos WHERE id_presupuesto='$idPresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function listar_sucursales()
    {
        $sql="SELECT * FROM sucursales";
        return ejecutarConsulta($sql);
    }
}