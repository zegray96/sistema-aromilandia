<?php
require('../config/connection.php');

class Configuracion
{
    public function __construct()
    {
    }

    public function traer_configuracion()
    {
        $sql="SELECT * FROM configuracion WHERE id_configuracion=1";
        return ejecutarConsultaSimpleFila($sql);
    }


    public function editar($nombreEmpresa, $compraMinimaMayorista, $siguientesComprasMayoristas)
    {
        $sql="UPDATE configuracion SET nombre_empresa = '$nombreEmpresa', compra_minima_mayorista='$compraMinimaMayorista', siguientes_compras_mayoristas='$siguientesComprasMayoristas' WHERE id_configuracion = 1";
        return ejecutarConsulta($sql);
    }
}